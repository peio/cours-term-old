---
author: Gilles Lassus, Nicolas Reveret, Pierre Marquestaut
title: Le langage SQL - les sélections sur plusieurs tables
---

# Langage SQL - les sélections sur plusieurs tables



On s'intéresse ici à la base de données `films` regroupant diverses informations sur des films :

* titre,
* année de réalisation,
* nom du réalisateur,
* résumé,
* nom et rôle des acteurs,
* note attribuée par des internautes,
* *etc*...

Les différentes tables sont représentées ci-dessous :

![La base `films`](images/films.png#only-light)
![La base `films`](images/films_dark.png#only-dark)

Sur cette figure :

* chaque tableau correspond à une table dont le nom est indiqué sur la première ligne ;

* les lignes suivantes listent les attributs et leur type. `varchar(255)` signifie que l'attribut est un texte de 255 caractères au maximum ;

* les clés primaires de chaque table sont indiquées en gras. Notez que la table `roles` a une clé primaire multiple ;

* les clés étrangères sont représentées par des liaisons entre les tables. On ne détaille pas ici la signification des nombres et des symboles aux extrémités des liaisons. Les curieux pourront se reporter à [cette page](https://en.wikipedia.org/wiki/Entity%E2%80%93relationship_model#Crow's_foot_notation).

Cette base est téléchargeable au format *sqlite* [ici](films.db). La source est [là](http://deptfod.cnam.fr/bd/tp/datasets/). Le fichier d'origine a été complété par nos soins. Ainsi, les internautes et les notes associées sont des [faux](https://pypi.org/project/Faker/).


## 1. Jointure de 2 tables

!!! abstract "Définition"
    Lorsque les informations sont stockées dans plusieurs tables, il est possible de n'effectuer qu'une seule requête en effectuant une **jointure**. Cette opération s'appuie sur les clés étrangères des tables et permet de mettre deux attributs en correspondance.

    La structure générale d'une jointure est :

    ```sql
    SELECT attribut
    FROM table_1
    JOIN table_2 ON table_1.attribut_1 = table_2.attribut_2;
    ```

!!! note "Plusieurs remarques"

    * on a précisé l'origine des attributs afin d'éviter les ambiguïtés si deux attributs de deux tables différentes ont le même nom. Ce n'est pas nécessaire s'il n'y a pas d'ambiguïté ;
    * il est possible de joindre d'autres tables en rajoutant des lignes `JOIN table_3 ...` ;
    * on peut tout à fait ajouter une clause `WHERE` à la suite des jointures ;
    * les attributs demandés peuvent provenir de toutes les tables jointes `SELECT table_1.attribut_1, table_2.attribut_2`.


!!! note "Jointure de 2 tables : JOIN"
    - **Commande :** 

    ```sql
        SELECT artistes.nom, artistes.prenom
        FROM artistes
        JOIN films ON films.idRealisateur = artistes.idArtiste
        WHERE films.titre = 'Casablanca';      
    ``` 
    - **Traduction :** 
    Comme plusieurs tables sont appelées, nous préfixons chaque colonne avec le nom de la table. Nous demandons ici le nom et le prénom du réalisateur de « *Casablanca* »

    {!{ sqlide titre="SQL" base="https://forge.apps.education.fr/peio/cours-term/-/raw/main/films.db?inline=false" espace="films" }!}

## 2. Jointure de 3 tables et alias


Les acteurs et actrices ne sont pas directement reliées au film, mais au travers de leur rôle.

(noms et prénoms) ont joué  ? Il va falloir joindre plus de deux tables...
    

        

!!! note "Jointure de 3 tables : JOIN"
    - **Commande :** 
    ```sql
    SELECT nom, prenom
        FROM artistes AS art
        JOIN roles ON roles.idActeur = art.idArtiste
        JOIN films ON roles.idFilm = films.idFilm
        WHERE films.titre = 'Sueurs froides';
    ``` 
    - **Traduction :** 
    On recherche ici le nom et prénom des acteurs et actrices ayant joué dans le film « *Sueurs froides* ».
    Il faut bien comprendre que la table principale qui nous intéresse ici est «emprunts», mais qu'on modifie les valeurs affichées en allant chercher des correspondances dans deux autres tables. 

    !!! note "Remarque"
        Notez ici que des alias sont donnés aux tables (par **AS**) afin de faciliter l'écriture. 


    - **Résultat :**  

    {!{ sqlide titre="SQL" espace="films" }!}


