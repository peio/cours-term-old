---
author: Gilles Lassus, Nicolas Reveret, Pierre Marquestaut
title: Le langage SQL - les sélections
---

# Langage SQL - les sélections


!!! abstract "Définition"
    **SQL** ou « **Structured Query Language** » est un langage informatique permettant de manipuler les données et les systèmes de bases de données relationnelles. Ce langage permet principalement de communiquer avec les bases de données afin de gérer les données qu’elles contiennent.

    Il permet notamment de stocker, de manipuler et de retrouver ces données. Il est aussi possible d’effectuer des requêtes, de mettre à jour les données, de les réorganiser, ou encore de créer et de modifier le schéma et la structure d’un système de base de données et de contrôler l’accès à ses données.

## 1. Différents moyens d'interroger la base de données

On considère dans ce sujet la base de données `prenoms`. Cette base ne comporte qu'une table,  `naissances`, qui contient des informations décrivant des prénoms des enfants nés en France au XX-ième siècle.

Cette base est téléchargeable au format *sqlite* [ici](https://forge.apps.education.fr/peio/cours-term/-/raw/main/prenoms.db?inline=false).

Les attributs de cette table sont :

* `id` : l'identifiant de l'entrée (clé primaire);
* `sexe` : le sexe de l'enfant au format texte : `Fille` ou `Garçon` ;
* `prenom` : le prénom de l'enfant en lettres majuscules ;
* `annee_naissance` : l'année de naissance (nombre entier) ;
* `nombre` : le nombre d'enfants nés durant l'année en question et portant ce prénom.



### 1.1 Requête basique 

!!! abstract "Structure"
    la structure générale d'une requête `SQL` est :

    ```sql
    SELECT attribut_1, attribut_2, ...
    FROM table
    WHERE condition;
    ```


!!! note "Requête basique : SELECT, FROM"

    - **Commande :** 
    ```sql
    SELECT * 
    FROM naissances 
    LIMIT 10;
    ``` 
    - **Traduction :** 

    On veut afficher dix premières lignes de la table "naissances". 

    - **Résultat :**   

    {!{ sqlide titre="SQL" base="https://forge.apps.education.fr/peio/cours-term/-/raw/main/prenoms.db?inline=false" init="02_bdd/scripts/init1.sql" espace="prenoms"}!}


    !!! tip "Astuce"

        Utiliser `LIMIT 10` à la fin de la requête permet de n'afficher que les 10 premiers résultats. Il y a beaucoup de lignes dans cette table !
        


!!! note "Remarques"

    - Les mots-clés SQL sont traditionnellement écrits en MAJUSCULES. 

    - Le `;` signale la fin de l'instruction. Il peut donc être omis s'il n'y a pas d'instructions enchaînées (ce qui sera toujours notre cas).  

    - L'indentation n'est pas syntaxique (pas comme en Python). On peut faire des retours à la ligne et des indentations pour rendre le code plus lisible.


### 1.2 La projection

!!! abstract "Définition"
    La projection consiste à ne retenir que certains attributs d’une table. 

    Le symbole `*` permet de sélectionner la totalité des attributs.

!!! note "Requête basique : SELECT, FROM"

    - **Commande :** 
    ```sql
    SELECT annee_naissance 
    FROM naissances
    LIMIT 10;;
    ``` 
    - **Traduction :** 

    On veut afficher uniquement les années de naissance des dix premières lignes de la table "prenoms". 

    - **Résultat :**   

    {!{ sqlide titre="SQL" base="https://forge.apps.education.fr/peio/cours-term/-/raw/main/prenoms.db?inline=false" init="02_bdd/scripts/init1.sql" espace="prenoms"}!}

!!! note "Plusieurs colonnes"
    - **Commande :** 
    ```sql
    SELECT annee_naissance, sexe 
    FROM naissances
    LIMIT 10;
    ``` 
    - **Traduction :** 

    On veut afficher le sexe et l'année de naissance des dix premières lignes de la table "prenoms". 

    - **Résultat :**   

    {!{ sqlide titre="SQL" base="https://forge.apps.education.fr/peio/cours-term/-/raw/main/prenoms.db?inline=false" init="02_bdd/scripts/init1.sql" espace="prenoms"}!}


### 1.3 Requête filtrée

!!! note "Requête filtrée : SELECT, FROM, WHERE"
    - **Commande :** 

    ```sql
    SELECT * 
    FROM naissances
    WHERE annee_naissance = 1923;
    ```
    
    Ou, en limitant le nombre de réponses  :

    ```sql
    SELECT * 
    FROM naissances
    WHERE annee_naissance = 1923
    LIMIT 10;
    ```

    - **Traduction :** 

    On veut les enregistrements des enfants nés durant l'année 1923.

    - **Résultat :**   

    {!{ sqlide titre="SQL" base="https://forge.apps.education.fr/peio/cours-term/-/raw/main/prenoms.db?inline=false" init="02_bdd/scripts/init2.sql" espace="prenoms"}!}


!!! note "Syntaxe"
    Le mot-clé **WHERE** doit être suivi d'un booléen. Les opérateurs classiques ```=``` , ```!=```, ```>```, ```>=```, ```<```, ```<=``` peuvent être utilisés, mais aussi le mot-clé **IN**


!!! note "Requête avec plusieurs possibilités : WHERE ... IN..."
    - **Commande :** 
    ```sql
    SELECT * 
    FROM naissances
    WHERE annee_naissance IN (1923, 1924, 1925);
    ``` 
    - **Traduction :** 

    On veut les titres de la table «livre» qui sont parus en 1990, 1991 ou 1992.

    - **Résultat :**   

    {!{ sqlide titre="SQL" base="https://forge.apps.education.fr/peio/cours-term/-/raw/main/prenoms.db?inline=false" init="02_bdd/scripts/init1.sql" espace="prenoms"}!}


!!! note "Opérateurs booléens"
    Il est également possible d'utiliser les opérateurs booléens `OR` et `AND` afin de créer des conditions composées.

!!! note "Requête avec booléens : AND - OR"
    - **Commande :** 
    ```sql
    SELECT COUNT(*)
    FROM naissances 
    WHERE annee_naissance = 1979 AND
          annee_naissance <= 1980 AND
          prenom = 'ANSELME';
    ``` 
    - **Traduction :** 

    On veut le nombre par année des enfants dénommés "ANSELME" nés entre 1970 et 1980.

    - **Résultat :**   

    {!{ sqlide titre="SQL" base="https://forge.apps.education.fr/peio/cours-term/-/raw/main/prenoms.db?inline=false" init="02_bdd/scripts/init1.sql" espace="prenoms"}!}



!!! note "Requête approchée : LIKE"
    - **Commande :** 
    ```sql
    SELECT nombre, annee_naissance
    FROM naissances 
    WHERE annee_naissance >= 1923 AND
          prenom LIKE "ANT%";
    ``` 
    - **Traduction :** 

    On veut les enregistrements de la table "naissances" des enfants nés en 1923 et dont le nom commence la chaîne de caractères "ANT".  
    Le symbole ```%``` est un joker qui peut symboliser n'importe quelle chaîne de caractères. 

    - **Résultat :**   

    {!{ sqlide titre="SQL" base="https://forge.apps.education.fr/peio/cours-term/-/raw/main/prenoms.db?inline=false" init="02_bdd/scripts/init1.sql" espace="prenoms"}!}





## 2. Opérations sur les données : sélection avec agrégation

Les requêtes effectuées jusqu'ici ont juste sélectionné des données grâce à différents filtres : aucune action à partir de ces données n'a été effectuée.  
Nous allons maintenant effectuer des opérations à partir des données sélectionnées. On appelle ces opérations des **opérations d'agrégation**.

### 2.1 Dénombrer 

!!! note "Compter : COUNT"
    - **Commande :** 
    ```sql
    SELECT COUNT(*)
    FROM naissances 
    WHERE annee_naissance = 1979;
    ``` 
    - **Traduction :** 

    On veut le nombre de prénoms donnés aux enfants nés en 1979.

    - **Résultat :**   

    {!{ sqlide titre="SQL" base="https://forge.apps.education.fr/peio/cours-term/-/raw/main/prenoms.db?inline=false" init="02_bdd/scripts/init1.sql" espace="prenoms"}!}

### 2.2 Somme

!!! note "Additionner : SUM"
    - **Commande :** 
    ```sql
    SELECT SUM(nombre)
    FROM naissances
    WHERE prenom = "Louise";
    ``` 
    - **Traduction :** 

    On veut additionner les années des livres de la tables livres comportant le mot "Astérix". Le résultat sera le seul élément d'une colonne nommée «somme».
    *Attention : dans notre cas précis, ce calcul n'a aucun sens...*

    - **Résultat :**   

    {!{ sqlide titre="SQL" base="https://forge.apps.education.fr/peio/cours-term/-/raw/main/prenoms.db?inline=false" init="02_bdd/scripts/init1.sql" espace="prenoms"}!}

### 2.3 Moyenne

!!! note "Faire une moyenne : AVG"
    - **Commande :** 
    ```sql
    SELECT AVG(nombre)
    FROM naissances
    WHERE prenom = "Louise"
    ``` 
    - **Traduction :** 

    On veut calculer la moyenne des années de parution des livres de la table livres comportant le mot "Astérix". Le résultat sera le seul élément d'une colonne nommée «moyenne».  
    *Attention : là encore, ce calcul n'a aucun sens...*

    - **Résultat :**   

     {!{ sqlide titre="SQL" init="02_bdd/scripts/init1.sql" espace="prenoms"}!}


### 2.4 Classement

!!! note "Classer des valeurs : ORDER BY, ASC, DESC, LIMIT :heart:"
    - **Commande :** 
    ```sql
    SELECT annee_naissance
    FROM naissances
    WHERE prenom =  "SATHINE"
    ORDER BY nombre DESC;
    ``` 

    - **Traduction :** 

    On veut afficher toutes les années où sont nés des enfants nommés "SATHINE", classées par nombre décroissant de naissances.
    
    - **Résultat :**   
    {!{ sqlide titre="SQL" init="02_bdd/scripts/init1.sql" espace="prenoms"}!}

!!! note "Comportement par défaut"
    Si le paramètre ASC ou DESC est omis, le classement se fait par ordre **croissant** (donc ASC est le paramètre par défaut).


### 2.5 Doublons 

!!! note "Suppression des doublons : DISTINCT"
    - **Commande :** 
    ```sql
    SELECT DISTINCT prenom
    FROM naissances 
    WHERE prenom LIKE "ANT%";
    ``` 
    - **Traduction :** 

    On veut la liste de tous les éditeurs. Sans le mot-clé `DISTINCT`, beaucoup de doublons apparaîtraient.

    - **Résultat :**   

    {!{ sqlide titre="SQL" init="02_bdd/scripts/init1.sql" espace="prenoms"}!}
