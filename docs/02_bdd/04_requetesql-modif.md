---
author: Gilles Lassus, Nicolas Reveret, Pierre Marquestaut
title: Le langage SQL - Modification d'une base de données
---

# Langage SQL - Modification d'une base de données


On considère dans ce sujet la base de données `netflix` contenant des informations sur les programmes de la plateforme. Cette base contient plusieurs relations/tables :

=== "Les pays"

    * `countries` : contient des informations décrivant des pays :

        * `id` : l'identifiant du pays (entier, clé primaire)

        * `country` : le nom du pays (texte)

=== "Les réalisateurs"

    * `directors` : contient des informations décrivant les réalisateurs des programmes :

        * `id` : l'identifiant du réalisateur (entier, clé primaire)

        * `director` : le nom du ou des réalisateurs (texte)

=== "Les genres"

    * `genres` : décrit les genres des programmes :

        * `id` : l'identifiant du genre (entier, clé primaire)

        * `genre` : le nom du genre (texte)

=== "Les programmes"

    * `programs` : décrit les programmes :

        * `show_id` : l'identifiant du programme (texte, clé primaire)

        * `type` : l'identifiant du type de programme (entier, clé étrangère pointant vers `types.id`)

        * `title` : le nom du programme (texte)

        * `director` : l'identifiant du ou des réalisateurs (entier, clé étrangère pointant vers `directors.id`)

        * `country` : l'identifiant du pays du programme (entier, clé étrangère pointant vers `countries.id`)

        * `date_added` : la date d'ajout sur la plateforme (texte)
        
        * `release_year` : la date de réalisation du programme (entier)

        * `ratings` : l'identifiant de la classification du programme (entier, clé étrangère pointant vers `ratings.id`)
        
        * `duration` : la durée du programme en saison ou minutes (texte)

=== "Les classifications"

    * `ratings` : décrit les classifications des programmes :

        * `id` : l'identifiant de la classification (entier, clé primaire)

        * `rating` : le nom de la classification (texte)

=== "Les genres de chaque programme"

    * `show_genres` : décrit les genres associés à chaque programme :

        * `show_id` : l'identifiant du programme (texte, clé étrangère pointant vers `programs.show_id`)

        * `genre` : l'identifiant du genre (entier, clé étrangère pointant vers `genres.id`)

        * La clé primaire de cette table est le couple `(show_id, genre)`.

=== "Les types de programmes"

    * `types` : décrit les types de programmes :

        * `id` : l'identifiant du type (entier, clé primaire)

        * `type` : le nom du type (texte)


Cette base est téléchargeable au format *sqlite* [ici](https://forge.apps.education.fr/peio/cours-term/-/raw/main/netflix.db?inline=false). La source est sur [kaggle](https://www.kaggle.com/datasets/d93a07b8b707e8e21e8fa542535533def4a4ffe24ed78215f65ce2851db58492?).


#### 1 Insertion d'enregistrement

!!! abstract "Structure"
    la structure générale d'une requête `SQL` d'insertion est :

    ```sql
    INSERT INTO table (attribut_1, attribut_2, ...)
    VALUES (valeur_1, valeur_2, ...);
    ```



!!! note "Insertion de valeurs : INSERT INTO, VALUES"
    - **Commande :** 
    ```sql
    INSERT INTO programs (title, release_year, director, rating)
    VALUES ('The Sandman', 2022, 1238, 9);
    ``` 
    - **Traduction :**
    On souhaite insérer la série télévisée `The Sandman`, ajoutée en 2022, de réalisateurs multiples (utiliser l'identifiant correspondant à `Not Given`), et de classification `TV-MA`. Les autres champs sont laissés vierges.

    L'identifiant de réalisateur est le $1238$, celui de la classification `TV-MA` est $9$.

    - **Résultat :**  

    {!{ sqlide titre="SQL" base="https://forge.apps.education.fr/peio/cours-term/-/raw/main/netflix.db?inline=false" espace="netflix"}!}

#### 2 Intérêt de la clé primaire

Essayons d'insérer un réalisateur ayant le même `id` qu'un autre réalisateur. 

- **Commande :** 
```sql
INSERT INTO directors VALUES  (1298, 'Ed Wood');
``` 

- **Résultat :**      
{!{ sqlide titre="SQL" espace="netflix"}!}

La contrainte de relation est violée : le SGBD «protège» la base de données en n'acceptant pas la proposition d'insertion. La base de données n'est pas modifiée.


!!! note "Remarque"
    Il est possible de «déléguer» la gestion des valeurs de la clé primaire avec l'instruction ```AUTOINCREMENT```. 

    L'attribut `id` est alors géré automatiquement par le SGBD.


#### 3 Modification

!!! abstract "Structure"
    la structure générale d'une requête `SQL` d'insertion est :

    ```sql
    UPDATE table
    SET attribut_1 = valeur_1
    WHERE condition;
    ```


4. La série `The Witcher` comporte désormais 2 saisons alors que la base n'en indique qu'une seule. Corriger cette erreur.


!!! note "Modification d'une valeur UPDATE, SET"
    Pour modifier le nombre de saisons :

    - **Commande :** 
    ```sql
    UPDATE programs SET duration = "2 Seasons" WHERE title = 'The Witcher';
    ``` 

    - **Traduction**
    La série `The Witcher` comporte désormais 2 saisons alors que la base n'en indique qu'une seule. On modifie cette erreur.


    - **Résultat :**    

    {!{ sqlide titre="SQL" espace="netflix"}!}


#### 4 Suppression

!!! abstract "Structure"
    la structure générale d'une requête `SQL` de suppression est :

    ```sql
    DELETE FROM table
    WHERE condition;
    ```

!!! note "Suppression d'un enregistrement : DELETE"


    Le film "Bad Boys II" sort du catalogue de la plateforme. Pour supprimer totalement la ligne concernant ce film :

    - **Commande :** 
    ```sql
    DELETE FROM programs WHERE title = 'Bad Boys II';
    ``` 

    - **Résultat :**     
     
    {!{ sqlide titre="SQL" espace="netflix"}!}

    Si une autre table contient par exemple l'attribut la clé primaire de la table programme comme clé étrangère, et si l'identifiant  du film "Bad Boys II" fait partie de cette table, le SGBD peut refuser de supprimer cette ligne, afin de ne pas violer la contrainte de référence.

!!! question "Mots clés"

    Ci-dessous se trouvent des rappels sur le vocabulaire des données structurées.

    <iframe id="mot"
        title="Mots clés"
        width="100%"
        height="570"
        src="{{ page.canonical_url }}../activites/vocabulaireSQL.html">
    </iframe>

!!! question "Mots clés associés"

    Ci-dessous se trouvent des rappels sur le vocabulaire des données structurées.

    <iframe id="associés"
        title="Mots clés associés"
        width="100%"
        height="450"
        src="{{ page.canonical_url }}../activites/association.html">
    </iframe>