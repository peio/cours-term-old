---
author: Pierre Marquestaut, Gilles Lassus, Jean-Louis Thirot , Mireille Coilhac, Valérie Mousseaux
title: Arbres - ABR
tags:
  - 8-arbre
---

## I. Objectifs des ABR ou **A**rbres **B**inaires de **R**echerche

!!! info "ABR"

    👉 Un arbre binaire de recherche (ABR) est un arbre binaire dans lequel chaque nœud possède une clé, telle que chaque nœud du sous-arbre gauche ait une clé strictement inférieure à celle du nœud considéré, et que chaque nœud du sous-arbre droit possède une clé supérieure ou égale à celle-ci — selon la mise en œuvre de l'ABR, on pourra interdire ou non des clés de valeur égale.
    
    😀 Un arbre binaire de recherche permet des opérations rapides pour rechercher une clé, insérer ou supprimer une clé. 

    > Source : https://fr.wikipedia.org/wiki/Arbre_binaire_de_recherche

    ![ABR 1](img/ABR_1.png){ width=80%; : .center }

    > Source de l'image : http://ressources.unisciel.fr/algoprog/s46bst/emodules/br00macours1/res/br00cours-texte-xxx.pdf

!!! info "Définition"

     En informatique, un arbre binaire de recherche ou ABR (en anglais, **b**inary **s**earch **t**ree ou **BST**) est une structure de données représentant un ensemble dont les clés appartiennent à un ensemble totalement ordonné.

    Les opérations caractéristiques sur les arbres binaires de recherche sont l’**insertion**, la **suppression**, et la **recherche** d’une valeur. Ces opérations sont peu couteuses si l’arbre n’est pas trop déséquilibré.

    💡 En pratique, les valeurs sont des clés permettant d’accéder à des enregistrements. 

!!! abstract "Définition d'un ABR"

    Un **arbre binaire de recherche** est un arbre binaire dont les valeurs des nœuds (valeurs qu'on appelle étiquettes, ou clés) vérifient la propriété suivante :

    - l'étiquette d'un nœud est **supérieure ou égale** à celle de **chaque** nœud de son **sous-arbre gauche**.
    - l'étiquette d'un nœud est **strictement inférieure** à celle du **chaque** nœud de son **sous-arbre droit**.




???+ question "ABR ou pas ABR"

    ![exABR.png](img/exABR.png){ width=60%; : .center }

    === "Parmi ces arbres, quels sont ceux qui sont des ABR ? (Cocher la réponse correcte)"
        
        - [ ] arbre 1
        - [ ] arbre 2
        - [ ] arbre 3
        - [ ] arbre 4
        - [ ] arbre 5      

    === "Solution"
        
        - :white_check_mark: c'est un ABR.
        - :x: L'arbre 2 n'est pas un ABR à cause de la feuille 6, qui fait partie du sous-arbre gauche de 5  sans lui être inférieure.
        - :white_check_mark: À noter que l'arbre 3 (qui est bien un ABR) est appelé **arbre filiforme**. 
        - :white_check_mark: c'est un ABR.
        - :x: L'arbre 5 n'est pas un ABR à cause de la feuille 9, qui fait partie du sous-arbre gauche de 3 sans lui être inférieure.

!!! note "Remarque :"

    On pourrait aussi définir un ABR comme un arbre dont le parcours infixe est une suite croissante.


## II Rechercher une clé dans un ABR

### II.1 Algorithme

Un arbre binaire de taille $n$ contient $n$ clés (pas forcément différentes). Pour savoir si une valeur particulière fait partie des clés, on peut parcourir tous les nœuds de l'arbre, jusqu'à trouver (ou pas) cette valeur dans l'arbre. Dans le pire des cas, il faut donc faire $n$ comparaisons.

Mais si l'arbre est un ABR, le fait que les valeurs soient «rangées» va considérablement améliorer la vitesse de recherche de cette clé, puisque la moitié de l'arbre restant sera écartée après chaque comparaison.

!!! note "Recherche d'une clé dans un ABR :heart:"
    
    <iframe src="https://www.codepuzzle.io/IPGJNZY" width="100%" height="600" frameborder="0"></iframe>
    

**Exemple** 

L'arbre `a` contient la valeur 8, mais l'arbre `b` ne la contient pas :



```python
>>> contient_valeur(a, 8)
True
>>> contient_valeur(b, 8)
False
```


### II.2  Coût de la recherche dans un ABR équilibré
![](img/rechercheABR.png){: .center}

Imaginons un arbre équilibré de taille $n$. Combien d'étapes faudra-t-il, dans le pire des cas, pour trouver (ou pas) une clé particulière dans cet arbre ?

Après chaque nœud, le nombre de nœuds restant à explorer est divisé par 2. On retrouve là le principe de recherche dichotomique.

S'il faut parcourir tous les étages de l'arbre avant de trouver (ou pas) la clé recherchée, le nombre de nœuds parcourus est donc égal à la hauteur $h$ de l'arbre.

Pour un arbre complet, cette hauteur vérifie la relation $2^h -1= n$. et donc $2^h = n+1$.

$h$ est donc le «nombre de puissance de 2» que l'on peut mettre dans $n+1$. Cette notion s'appelle le logarithme de base 2 et se note $\log_2$.

Par exemple, $\log_2(64)=6$ car $2^6=64$.

Le nombre maximal de nœuds à parcourir pour rechercher une clé dans un ABR équilibré de taille $n$ est donc de l'ordre de $\log_2(n)$, ce qui est très performant !

Pour arbre contenant 1000 valeurs, 10 étapes suffisent.

Cette **complexité logarithmique** est un atout essentiel de la structure d'arbre binaire de recherche.

## III Insertion dans un ABR
L'insertion d'une clé va se faire au niveau d'une feuille, donc au bas de l'arbre. Dans la version récursive de l'algorithme d'insertion, que nous allons implémenter, il n'est pourtant pas nécessaire de descendre manuellement dans l'arbre jusqu'au bon endroit : il suffit de distinguer dans lequel des deux sous-arbres gauche et droit doit se trouver la future clé, et d'appeler récursivement la fonction d'insertion dans le sous-arbre en question.

**Algorithme :**

- Si l'arbre est vide, on renvoie un nouvel objet Arbre contenant la clé.
- Sinon, on compare la clé à la valeur du nœud sur lequel on est positionné :
    - Si la clé est inférieure à cette valeur, on va modifier le sous-arbre gauche en le faisant pointer vers ce même sous-arbre une fois que la clé y aura été injectée, par un appel récursif.
    - Si la clé est supérieure, on fait la même chose avec l'arbre de droite.
    - on renvoie le nouvel arbre ainsi créé.

!!! note "Insertion dans un ABR :heart:"

    <iframe src="https://www.codepuzzle.io/IP89PAW" width="100%" height="600" frameborder="0"></iframe>



**Exemple :** Nous allons insérer la valeur 4 dans l'arbre ```a``` et vérifier par un parcours infixe (avant et après l'insertion) que la valeur 4 a bien été insérée au bon endroit.

![](img/insertionABR.png){: .center}


```python
a = Arbre(5)
a.left = Arbre(2)
a.right = Arbre(7)
a.left.left = Arbre(0)
a.left.right = Arbre(3)
a.right.left = Arbre(6)
a.right.right = Arbre(8)
```


```python
>>> infixe(a)
0-2-3-5-6-7-8-
>>> a = insertion(a,4)
<__main__.Arbre at 0x7f46f0507e80>
>>> infixe(a)
0-2-3-4-5-6-7-8-
```


La valeur 4 a donc bien été insérée au bon endroit.



---
## Bibliographie
- Numérique et Sciences Informatiques, Terminale, T. BALABONSKI, S. CONCHON, J.-C. FILLIATRE, K. NGUYEN, éditions ELLIPSES.





