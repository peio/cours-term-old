---
author: Cédric Gouygou, Gilles Lassus et Mireille Coilhac
title: Les graphes
---


## I. Notion de graphe et vocabulaire

Le concept de graphe permet de résoudre de nombreux problèmes en mathématiques comme en informatique. C'est un outil de représentation très courant, et nous l'avons déjà rencontré à plusieurs reprises, en particulier lors de l'étude de réseaux.


### 1.1 Exemples de situations

#### 1.1.1 Réseau informatique

![22J2AS1_ex2.png](img/22J2AS1_ex2.png){ width=30% }


#### 1.1.2 Réseau de transport

![carte-metro-parisien-768x890.jpg](img/carte-metro-parisien-768x890.jpg){ width=40% }

#### 1.1.3 Réseau social

![graphe_RS.png](img/graphe_RS.png){ width=30% }

#### 1.1.4 Poster du courrier

Vous êtes le facteur d'un petit village, vous distribuez le courrier à vélo, à la seule force de vos jambes. Vous devez passer devant toutes les maisons du village, ce qui implique de traverser toutes les rues. Mais soucieux de préserver vos forces et de renouveler continuellement votre découverte des paysages, vous ne voulez pas traverser deux fois la même rue. Ici chaque nœud est un carrefour, et chaque arête une rue. Vous êtes en train de chercher un **circuit Eulérien** !

Il doit son nom à Leonhard Euler, qui chercha à savoir s'il était possible de franchir les 7 ponts de Königsberg sans jamais repasser deux fois sur le même (et en ne traversant le fleuve que grâce aux ponts, bien entendu).

<div class="milieu" style="margin-left:10px;">
<img alt="Les 7 ponts de Königsberg" src="http://zestedesavoir.com/media/galleries/912/f16ed77f-92ef-4a07-bd8f-e41f37ffff48.png.960x960_q85.jpg">  </p>
</div>
*Source de l'image : http://zestedesavoir.com*



#### 1.1.5 Généralisation
Une multitude de problèmes concrets d'origines très diverses peuvent donner lieu à des modélisations par des graphes : c'est donc une structure essentielle en sciences, qui requiert un formalisme mathématique particulier que nous allons découvrir. 

![graph_math.png](img/graph_math.png){ width=50% }

L'étude de la théorie des graphes est un champ très vaste des mathématiques : nous allons surtout nous intéresser à l'implémentation en Python d'un graphe et à différents problèmes algorithmiques qui se posent dans les graphes.




### 1.2 Vocabulaire

!!! abstract "Définition"
    En général, un graphe est un ensemble d'objets, appelés *sommets* ou parfois *nœuds* (*vertex* or *nodes* en anglais) reliés par des *arêtes* ou *arcs* (*edges* en anglais).

Ce graphe peut être **non-orienté** ou **orienté** .

#### 1.2.1 Graphe non-orienté

![exemple_graphe.png](img/exemple_graphe.png){ width=30% }

!!! abstract "Définition"
    Dans un graphe **non-orienté**, les *arêtes* peuvent être empruntées dans les deux sens, et une *chaîne* est une suite de sommets reliés par des arêtes, comme C - B - A - E par exemple. La *longueur* de cette chaîne est alors 3, soit le nombre d'arêtes.

Les sommets B et E sont *adjacents* au sommet A, ce sont les *voisins* de A.


**Exemple de graphe non-orienté** : le graphe des relations d'un individu sur Facebook est non-orienté, car si on est «ami» avec quelqu'un la réciproque est vraie.

#### 1.2.2 Graphe orienté

![exemple_graphe_oriente.png](img/exemple_graphe_oriente.png){ width=30% }

!!! abstract "Définition"
    Dans un graphe **orienté**, les *arcs* ne peuvent être empruntés que dans le sens de la flèche, et un *chemin* est une suite de sommets reliés par des arcs, comme B → C → D → E par exemple.

Les sommets C et D sont *adjacents* au sommet B (mais pas A !), ce sont les *voisins* de B.

**Exemple de graphe orienté** : le graphe des relations d'un individu sur Twitter est orienté, car on peut «suivre» quelqu'un sans que cela soit réciproque.

#### 1.2.3 Graphe pondéré

![exemple_graphe_pondere.png](img/exemple_graphe_pondere.png){ width=30% }

!!! abstract "Définition"
    Un graphe est **pondéré** (ou valué) si on attribue à chaque arête une valeur numérique (la plupart du temps positive), qu'on appelle *mesure*, *poids*, *coût* ou *valuation*.

Par exemple:

- dans le protocole OSPF, on pondère les liaisons entre routeurs par le coût;
- dans un réseau routier entre plusieurs villes, on pondère par les distances.


#### 1.2.4 Connexité

!!! abstract "Définition"
    Un graphe est **connexe** s'il est d'un seul tenant: c'est-à-dire si n'importe quelle paire de sommets peut toujours être reliée par une chaîne. Autrement un graphe est connexe s'il est «en un seul morceau».

Par exemple, le graphe précédent est connexe. Mais le suivant ne l'est pas: il n'existe pas de chaîne entre les sommets A et F par exemple.

![exemple_graphe_non_connexe.png](img/exemple_graphe_non_connexe.png){ width=30% }

Il possède cependant deux **composantes connexes** : le sous-graphe composé des sommets A, B, C, D et E d'une part et le sous-graphe composé des sommets F, G et H.


## II. Modélisations d'un graphe

Pour modéliser un graphe, il faut établir par convention une manière de donner les renseignements suivants :

- qui sont les sommets ?
- pour chaque sommet, quels sont ses voisins ? (et éventuellement quel poids porte l'arête qui les relie)


### 2.1 Représentation par matrice d'adjacence

!!! abstract "Principe"
    - On classe les sommets (en les numérotant, ou par ordre alphabétique).
    - on représente les arêtes (ou les arcs) dans une matrice, c'est-à-dire un tableau à deux dimensions où on inscrit un 1 en ligne `i` et colonne `j` si les sommets de rang `i` et de rang `j` sont **voisins** (dits aussi *adjacents*).

    Ce tableau s'appelle une **matrice d'adjacence** (on aurait très bien pu l'appeler aussi *matrice de voisinage*).


#### 2.1.1 Graphe non orienté


![matgraph_1.png](img/matgraph_1.png){ width=50% }


Dans ce graphe non orienté, comme B est voisin de C, C est aussi voisin de B, ce qui signifie que l'arête qui relie B et C va donner lieu à deux "1" dans la matrice, situé de part et d'autre de la diagonale descendante (un mathématicien parlera de matrice *symétrique*).


#### 2.1.2 Graphe orienté

![matgraph_2.png](img/matgraph_2.png){ width=50% }


#### 2.1.3 Graphe pondéré

![matgraph_3.png](img/matgraph_3.png){ width=55% }


???+ question "Exercice 1"

    Soit un ensemble d'amis connectés sur un réseau social quelconque. Voici les interactions qu'on a recensées:

    - André est ami avec Béa, Charles, Estelle et Fabrice,
    - Béa est amie avec André, Charles, Denise et Héloïse,
    - Charles est ami avec André, Béa, Denise, Estelle, Fabrice et Gilbert,
    - Denise est amie avec Béa, Charles et Estelle,
    - Estelle est amie avec André, Charles et Denise,
    - Fabrice est ami avec André, Charles et Gilbert,
    - Gilbert est ami avec Charles et Fabrice,
    - Héloïse est amie avec Béa.
    
    **Q1.** Représenter le graphe des relations dans ce réseau social (on désignera chaque individu par l'initiale de son prénom). Il est possible de faire en sorte que les arêtes ne se croisent pas !

    ??? success "Solution Q1"

        ![grapheRS](img/grapheRS.png){ width=30%; : .center }

        
    **Q2.** Donner la matrice d'adjacence de ce graphe.

    ??? success "Solution Q2"

        $\pmatrix{
        0 & 1 & 1 & 0 & 1 & 1 & 0 & 0 \\
        1 & 0 & 1 & 1 & 0 & 0 & 0 & 1 \\
        1 & 1 & 0 & 1 & 1 & 1 & 1 & 0 \\
        0 & 1 & 1 & 0 & 1 & 0 & 0 & 0 \\
        1 & 0 & 1 & 1 & 0 & 0 & 0 & 0 \\
        1 & 0 & 1 & 0 & 0 & 0 & 1 & 0 \\
        0 & 0 & 1 & 0 & 0 & 1 & 0 & 0 \\
        0 & 1 & 0 & 0 & 0 & 0 & 0 & 0 \\
        }$
        
        
???+ question "Exercice 2"

    Construire les graphes correspondants aux matrices d'adjacence suivantes:

    **Q1.** $M_1 =\pmatrix{
        0&1&1&1&1\\
        1&0&1&0&0\\
        1&1&0&1&0\\
        1&0&1&0&1\\
        1&0&0&1&0\\
        }$

    ??? success "Solution Q1"

        ![ex2_Q1.png](img/ex2_Q1.png){ width=20%; : .center }
        
    **Q2.** $M_2=\pmatrix{
        0&1&1&0&1\\
        0&0&1&0&0\\
        0&0&0&1&0\\
        1&0&0&0&1\\
        0&0&0&0&0\\
        }$
    
    ??? success "Solution Q2"

        ![ex2_Q2.png](img/ex2_Q2.png){ width=20%; : .center }


    **Q3.** $M_3=\pmatrix{
        0&5&10&50&12\\
        5&0&10&0&0\\
        10&10&0&8&0\\
        50&0&8&0&100\\
        12&0&0&100&0\\
        }$    

    ??? success "Solution Q3"

        ![ex2_Q3.png](img/ex2_Q3.png){ width=22%; : .center }


#### 2.1.5 Implémentation Python des matrices d'adjacence

!!! info "Matrices d'adjacence en Python"
    Une matrice se représente naturellement par une liste de listes.

    **Exemple:**
    La matrice $M_1 =\pmatrix{
        0&1&1&1&1\\
        1&0&1&0&0\\
        1&1&0&1&0\\
        1&0&1&0&1\\
        1&0&0&1&0\\
        }$, associée au graphe suivant
        
    ![ex2_Q1.png](data/ex2_Q1.png){ width=20% }

    sera représentée par la variable ```G``` suivante :

    ```python
    G = [[0, 1, 1, 1, 1],
          [1, 0, 1, 0, 0],
          [1, 1, 0, 1, 0],
          [1, 0, 1, 0, 1],
          [1, 0, 0, 1, 0]]
    ```

**Complexité en mémoire et temps d'accès :**

- Pour un graphe à $n$ sommets, la complexité en mémoire (appelée aussi *complexité spatiale*) de la représentation matricielle est en $O(n^2)$.

- Tester si un sommet est isolé (ou connaître ses voisins) est en $O(n)$ puisqu'il faut parcourir une ligne, mais tester si deux sommets sont adjacents (voisins) est en $O(1)$, c'est un simple accès au tableau.



La modélisation d'un graphe par sa matrice d'adjacence est loin d'être la seule manière de représenter un graphe : nous allons voir une autre modélisation, par **liste d'adjacence**.

### 2.2 Représentation par listes d'adjacence

!!! info "Principe"

    - On associe à chaque sommet sa liste des voisins (c'est-à-dire les sommets adjacents). On utilise pour cela un dictionnaire dont les clés sont les sommets et les valeurs les listes des voisins.

    - Dans le cas d'un graphe orienté on associe à chaque sommet la liste des *successeurs* (ou bien des *prédécesseurs*, au choix).

    Par exemple, le graphe 
    
    ![ex2_Q1.png.png](img/ex2_Q1.png){ width=20%; : .center } 
    sera représenté par le dictionnaire :


    ```python linenums='1'
    G = {'A': ['B', 'C', 'D', 'E'],
         'B': ['A', 'C'],
         'C': ['A', 'B', 'D'],
         'D': ['A', 'C', 'E'],
         'E': ['A', 'D']
        }
    ```

**Complexité en mémoire et temps d'accès :**

- Pour un graphe à $n$ sommets et $m$ arêtes, la complexité spatiale de la représentation en liste d'adjacence est en $O(n+m)$. C'est beaucoup mieux qu'une matrice d'adjacence lorsque le graphe comporte peu d'arêtes (i.e. beaucoup de 0 dans la matrice, non stockés avec des listes).

- Tester si un sommet est isolé (ou connaître ses voisins) est en $O(1)$ puisqu'on y accède immédiatement, mais tester si deux sommets sont adjacents (voisins) est en $O(n)$ car il faut parcourir la liste.

### 2.2.1 Exercices

???+ question "Exercice 3"

    Construire les graphes correspondants aux listes d'adjacence suivantes.

    **Q1.** 
    ```python
    G1 = {
    'A': ['B', 'C'],
    'B': ['A', 'C', 'E', 'F'],
    'C': ['A', 'B', 'D'],
    'D': ['C', 'E'],
    'E': ['B', 'D', 'F'],
    'F': ['B', 'E']
         }
    ```
    ??? success "Solution Q1"

        ![ex3_Q1.png](img/ex3_Q1.png){ width=20%; : .center }


    **Q2.** 
    ```python
    G2 = {
    'A': ['B'],
    'B': ['C', 'E'],
    'C': ['B', 'D'],
    'D': [],
    'E': ['A']
         }

    ```
    ??? success "Solution Q2"

        ![ex3_Q2.png](img/ex3_Q2.png){ width=20%; : .center }


## III. Création d'une classe `Graphe`

Dans cette partie, nous ne traiterons que des graphes **non-orientés**.

### Interface souhaitée

Nous voulons que le graphe ![image](img/ex2_Q1.png){: .center} puisse être créé grâce aux instructions suivantes :

```python
g = Graphe(['A', 'B', 'C', 'D', 'E'])
g.ajouter_arete('A', 'B')
g.ajouter_arete('A', 'C')
g.ajouter_arete('A', 'D')
g.ajouter_arete('A', 'E')
g.ajouter_arete('B', 'C')
g.ajouter_arete('C', 'D')
g.ajouter_arete('D', 'E')
```

???+ question "Une classe `Graphe`"

    ```python
    class Graphe:

    def __init__ (self, sommets):
        self.sommets = sommets
        self.dic = {sommet: [] for sommet in self.sommets} # création par compréhension
    
    def ajouter_arete(self, x, y):
        if y not in self.dic[x]:
            self.dic[x].append(y)
        if x not in self.dic[y]:
            self.dic[y].append(x)
    
    def get_sommets(self):
        return self.sommets
    
    def get_voisins(self, x):
        return self.dic[x]
        
    def get_dictionnaire(self):
        return self.dic
    ```

    A quoi voit-on que le graphe créé est non orienté ?

    ??? success "Solution"

        La fonction `ajouter_arete(self, x, y)`  ajoute, s'il n'y est pas déjà, `y ` dans la liste d'adjacence de `x`, et `x ` dans la liste d'adjacence de `y`.

## IV. Les parcours de graphes

Contrairement aux arbres, les graphes n’ont pas de racine, de « début ». On peut donc choisir n’importe
quel sommet pour commencer le parcours. Ceci dit, le parcours de certains graphes orientés demande un
choix de sommet de début réfléchi. 


!!! abstract "A retenir"

    * Parcours en largeur : **File**
    * Pacours en profondeur : **Pile**

### 4.1 Le parcours en largeur

!!! info "Parcours en largeur"

	Parcours en largeur ou **BFS** en anglais pour **B**readth **F**irst **S**earch.

    L'idée du parcours en largeur repose sur l'utilisation d'une file de la manière suivante :

	1. On part d'un sommet;
	2. On visite ses voisins directs et on les enfile s'ils ne sont pas déjà présents dans la file;
	3. On défile (c'est-à-dire qu'on enlève la tête de la file);
	4. On recommence à partir du point 2 tant que la file n'est pas vide.


	Voici la traduction en pseudo-code du parcours en largeur d'un graphe décrit ci-dessus.

	```txt title="Pseudo-code"
	F est une file vide
	On enfile un sommet
	Tant que F n'est pas vide
		S = Tête de F
		On enfile les voisins de S qui ne sont pas déjà présents dans la file et qui n'ont pas été déjà défilés
		On défile F
	Fin Tant Que
	```
On peut visualiser les étapes d'un parcours en largeur sur [cette page](https://workshape.github.io/visual-graph-algorithms/#bfs).

???+ question

    Ecrire le code d'une fonction `parcours_en_largeur` qui parcourt en largeur un graphe à partir du sommet `sommet` et dont sa liste d'adjacence graphe est representée par un dictionnaire nommé `graphe`

	Cette fonction renverra la liste des sommets parcourus si le sommet `sommet` appartient bien au graphe et `None` sinon.
    Compléter le script ci-dessous :

    {{ IDE('scripts/largeur') }}

!!! info "Distances croissantes"

    On peut remarquer que l'algorithme de parcours en largeur permet de dresser la liste des sommets d'un graphe par distance croissante au sommet d'origine

    ![largeur](img/largeur.png){ width=50% }

    A partir du sommet A représenté en bleu,on a par distance croissante :

	Les sommets à une distance de 1 du sommet A représentés en vert : B, D, E
	Les sommets à une distance de 2 du sommet A représentés en rouge : C, F, G
	Le sommet à une distance de 3 du sommet A représenté en jaune : H
	On retrouve bien dans la liste retournée par la fonction de parcours en largeur à partir du sommet A les sommets classés par ordre de distance croissante au sommet A : ['A', 'B', 'D', 'E', 'C', 'F', 'G', 'H'].

	💡 On peut se servir de ce parcours pour trouver un chemin de distance minimale entre deux sommets.


### 4.2 Le parcours en profondeur

!!! info "Parcours en profondeur"

	Parcours en profondeur ou **DFS** en anglais pour **D**epth **F**irst **S**earch

    L'idée du parcours en profondeur repose sur l'utilisation d'une pile de la manière suivante :

	1. On part d'un sommet que l'on empile;
	2. On dépile et on marque le sommet dépilé comme traité;
	3. On empile chacun des voisins su sommet dépilé qui ne sont pas déjà dans la pile et qui n'ont pas été déjà été traités;
	4. On recommence à partir du point 2 tant que la pile n'est pas vide.


	Voici la traduction en pseudo-code du parcours en profondeur d'un graphe décrit ci-dessus.

	```txt title="Pseudo-code"
	P est une pile vide
	On empile un sommet
	Tant que P n'est pas vide
		S = dépile(P)
		On empile les voisins de P qui ne sont pas déjà présents dans la pile et qui n'ont pas été déjà dépilés
	Fin Tant Que
	```
On peut visualiser les étapes d'un parcours en profondeur sur [cette page](https://workshape.github.io/visual-graph-algorithms/#dfs).




???+ question "A vous"

    Ecrire le code d'une fonction `parcours_en_profondeur` qui parcourt en largeur un graphe à partir du sommet `sommet` et dont sa liste d'adjacence graphe est representée par un dictionnaire nommé `graphe`

	Cette fonction renverra la liste des sommets parcourus si le sommet `sommet` appartient bien au graphe et `None` sinon.
    Compléter le script ci-dessous :

    {{ IDE('scripts/profondeur') }}
    
#### 4.1.1 Principe

**Exemple de parcours en largeur, avec B comme sommet de départ:**

<center>
<gif-player src="https://glassus.github.io/terminale_nsi/T1_Structures_de_donnees/1.4_Graphes/data/bfs.gif" speed="1" play></gif-player>
</center>

**Codes couleur :**

- **vert** : les sommets non encore traités.
- **rouge** : le sommet en cours de traitement.
- **orange** : la file d'attente des sommets qui seront bientôt traités. On y rajoute à chaque fois les voisins du sommet en cours de traitement, uniquement **si ils n'ont pas encore été découverts**.
- **noir** : les sommets traités.

#### 4.1.2 Algorithme BFS

On utilise :

- une liste `#!py traites` qui recueille les sommets visités (c'est-à-dire qu'on a fini de traiter, après avoir ajouté ses voisins dans la file d'attente) et qui sera renvoyée à la fin de l'algorithme;
- une liste `#!py decouverts` qui contient les sommets découverts au fur et à mesure du parcours;
- une **file** `#!py en_attente` qui contient les sommets découverts mais non encore visités. On utilisera au choix une classe `File` écrite plus tôt dans l'année ou tout simplement une `#!py list` en utilisant `#!py pop(0)` (pour défiler) et `#!py append()` (pour enfiler).

En début d'algorithme, seul le sommet de départ `#!py depart` donné en paramètre est découvert. La fonction `BFS` renvoie la liste des sommets dans l'ordre de visite lors du parcours en largeur.


{#
!!! abstract "Parcours en largeur - BFS :heart: :heart: :heart:"
    ```python linenums='1'
    def BFS(g, depart):
        '''
        Effectue un parcours en largeur du graphe g en partant du sommet depart,
        et renvoie la liste des sommets visités dans l'ordre du parcours.
        '''
        traites = []
        decouverts = [...]
        en_attente = [...]
        while ... != [] :
            sommet = ....pop(0)
            voisins = g.voisins(...)
            for voisin in ...:
                if voisin not in decouverts:
                    decouverts.append(voisin)
                    en_attente.append(voisin)
            traites.append(...)
        return traites

    ```
#}


!!! abstract "Parcours en largeur - BFS "
    ```python linenums='1'
    def BFS(g, depart):
        '''
        Effectue un parcours en largeur du graphe g en partant du sommet depart,
        et renvoie la liste des sommets visités dans l'ordre du parcours.
        '''
        traites = []
        decouverts = [depart]
        en_attente = [depart]
        while en_attente != [] :
            sommet = en_attente.pop(0)
            voisins = g.voisins(sommet)
            for voisin in voisins:
                if voisin not in decouverts:
                    decouverts.append(voisin)
                    en_attente.append(voisin)
            traites.append(sommet)
        return traites

    ```


!!! warning "Intérêt de la liste ```decouverts```"
    La liste ```decouverts``` contient tous les sommets qui ont été :

    - soit traités (auquel cas ils sont dans la liste ```traites```)
    - soit en attente (auquel cas ils sont dans la liste ```en_attente```)

    Le test de la ligne 13  `#!py if voisin not in decouverts:` permet donc de ne pas mettre en file d'attente un voisin qui est (ou a été) déjà en file d'attente. 

!!! warning "Que contient la file ```en_attente``` ?"
    À chaque instant, la file ```en_attente``` contient des sommets à la distance ```k+1``` et à la distance ```k``` du point de départ :

    ![image](img/en_attente.png){: .center}
     




!!! example "{{ exercice() }}"
    ![image](img/BFS_ex1.png){: .center}

    Grâce à la classe ```Graphe``` du 3.3, ce graphe s'implémente par :

    ```python linenums='1'
    g = Graphe(['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'])
    g.ajoute_arete('A', 'B')
    g.ajoute_arete('A', 'C')
    g.ajoute_arete('B', 'D')
    g.ajoute_arete('D', 'C')
    g.ajoute_arete('B', 'E')
    g.ajoute_arete('D', 'E')
    g.ajoute_arete('E', 'F')
    g.ajoute_arete('E', 'G')
    g.ajoute_arete('F', 'G')
    g.ajoute_arete('G', 'H')
    ```
    
    **Q1.** Donner le parcours en largeur de ```g```  grâce à l'algorithme BFS, si le sommet de départ est B. Cela correspond au parcours présenté par le gif de début de paragraphe.

    ??? tip "correction Q1"
        ```python
        >>> BFS(g, 'B')
        ['B', 'A', 'D', 'E', 'C', 'F', 'G', 'H']
        ```

    **Q2.** Deviner le parcours en largeur de départ D, puis de départ G. Vérifier grâce à votre algorithme.

    ??? tip "Correction Q2"
        ```python
        >>> BFS(g, 'D')
        ['D', 'B', 'C', 'E', 'A', 'F', 'G', 'H']
        >>> BFS(g, 'G')
        ['G', 'E', 'F', 'H', 'B', 'D', 'A', 'C']
        ```
    

### 4.1.3 Application du BFS : recherche du plus court chemin

L'algorithme BFS découvre les sommets «par cercles concentriques» autour du point de départ (ainsi que le montre la structure de la file d'attente). On découvre d'abord tous les sommets à la distance 1 du point de départ, puis à la distance 2, puis 3, etc.

Un sommet situé à la distance 5 sera découvert en tant que voisin d'un sommet à la distance 4, qui lui-même aura été découvert grâce à un sommet à la distance 3, qui lui-même...

On comprend donc que si on arrive à se souvenir du sommet «parent» de chaque sommet (celui qui lui a permis d'être découvert), on pourra alors reconstituer un chemin permettant de remonter au point de départ.

Nous allons pour cela nous servir d'une structure de dictionnaire pour associer à chaque sommet son sommet-parent.

Il faudra ensuite une fonction pour recréer le chemin.


!!! warning "Pourquoi le plus court chemin ?"

    - Comment est-on sûr qu'un chemin va être trouvé entre deux sommets A et B ?

    Si le graphe est connexe, tout parcours BFS au départ de A va parcourir l'intégralité du graphe, et donc passera par B à un moment. Un chemin sera donc forcément trouvé entre A et B.

    - Comment est-on sûr que ce chemin trouvé est **le plus court** ?

    La découverte des sommets par cercles concentriques entre A et B nous assure qu'on ne peut pas rater le point B : s'il est à la distance ```k``` de A, il sera forcément visité puisque tous les sommets à la distance ```k``` vont passer par la liste d'attente, après les sommets de distance ```k-1``` et avant les sommets de distance ```k+1```.
    
    Lorsqu'on remontera de B vers A en passant par les sommets parents successifs, il ne peut y avoir qu'un seul sommet par «couche» : le chemin sera donc exactement de longueur ```k```, il sera donc minimal. 


!!! abstract "Recherche du plus court chemin "

    {#
    ```python linenums='1'
    def recherche_chemin(g, depart, arrivee):
        '''
        Parcours en largeur du graphe g en partant du sommet depart,
        qui s'arrête dès que le sommet arrivee est atteint.
        Renvoie alors le chemin du depart vers arrivee.
        '''
        traites = []
        decouverts = [depart]
        en_attente = [depart]
        parent = {}
        while en_attente != [] :
            sommet = en_attente.pop(0)
            voisins = g.voisins(sommet)
            for voisin in voisins:
                if voisin not in decouverts:
                    decouverts.append(voisin)
                    en_attente.append(voisin)
                    parent[voisin] = sommet
                    if voisin == arrivee:
                        return remonte_chemin(depart, arrivee, parent)
            traites.append(sommet)
        return "non trouvé"  


    def remonte_chemin(depart, arrivee, parent):
        sommet = arrivee
        chemin = arrivee
        while sommet != ...:
            sommet = parent[...]
            chemin = ... + chemin
        return chemin
    ```
    #}


    ```python linenums='1'
    def recherche_chemin(g, depart, arrivee):
        '''
        Parcours en largeur du graphe g en partant du sommet depart,
        qui s'arrête dès que le sommet arrivee est atteint.
        Renvoie alors le chemin du depart vers arrivee.
        '''
        traites = []
        decouverts = [depart]
        en_attente = [depart]
        parent = {}
        while en_attente != [] :
            sommet = en_attente.pop(0)
            voisins = g.voisins(sommet)
            for voisin in voisins:
                if voisin not in decouverts:
                    decouverts.append(voisin)
                    en_attente.append(voisin)
                    parent[voisin] = sommet
                    if voisin == arrivee:
                        return remonte_chemin(depart, arrivee, parent)
            traites.append(sommet)
        return "non trouvé"  


    def remonte_chemin(depart, arrivee, parent):
        sommet = arrivee
        chemin = arrivee
        while sommet != depart:
            sommet = parent[sommet]
            chemin = sommet + chemin
        return chemin
    ```



!!! example "{{ exercice() }}"
    ![image](img/BFS_ex1.png){: .center}
    Tester le code précédent pour trouver le plus court chemin entre A et G, entre H et C, entre B et G...


### 4.2 Le parcours en profondeur (DFS, Depth First Search)

#### 4.2.1 Parcours DFS récursif


Le parcours en profondeur est un parcours où on va aller «le plus loin possible» sans se préoccuper des autres voisins non visités : on va visiter le premier de ses voisins non traités, qui va faire de même, etc. Lorsqu'il n'y a plus de voisin, on revient en arrière pour aller voir le dernier voisin non visité.

Dans un labyrinthe, ce parcours s'explique très bien : on prend tous les chemins sur la droite jusqu'à rencontrer un mur, auquel cas on revient au dernier embranchement et on prend un autre chemin, puis on repart à droite, etc.



C'est un parcours qui s'écrit naturellement de manière **récursive** :

{#
!!! abstract "Parcours en profondeur - DFS :heart: :heart: :heart:"
    ```python linenums='1'
    def DFSrec(g, traites, actuel):
        traites.append(...)
        for voisin in ...:
            if voisin not in ...:
                ...
        return traites
    ```
#}




!!! abstract "Parcours en profondeur - DFS :heart: :heart: :heart:"
    ```python linenums='1'
    def DFSrec(g, traites, actuel):
        traites.append(actuel)
        for voisin in g.voisins(actuel):
            if voisin not in traites:
                DFSrec(g, traites, voisin)
        return traites
    ```



!!! example "{{ exercice() }}"
    ![image](img/BFS_ex1.png){: .center}
    
    **Q1.** Donner (de tête) le parcours DFS de ce graphe en partant de A.  
    Rappel : les voisins sont donnés par ordre alphabétique. Le premier voisin de A est donc B.

    **Q2.** Vérifier avec le code précédent. 
    ??? tip "Correction Q2"
        ```python linenums='1'
        class Graphe:
            def __init__(self, liste_sommets):
                self.liste_sommets = liste_sommets
                self.adjacents = {sommet : [] for sommet in liste_sommets}

            def ajoute_arete(self, sommetA, sommetB):
                self.adjacents[sommetA].append(sommetB)
                self.adjacents[sommetB].append(sommetA)

            def voisins(self, sommet):
                return self.adjacents[sommet]

            def sont_voisins(self, sommetA, sommetB):
                return sommetB in self.adjacents[sommetA]


        g = Graphe(['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'])
        g.ajoute_arete('A', 'B')
        g.ajoute_arete('A', 'C')
        g.ajoute_arete('B', 'D')
        g.ajoute_arete('D', 'C')
        g.ajoute_arete('B', 'E')
        g.ajoute_arete('D', 'E')
        g.ajoute_arete('E', 'F')
        g.ajoute_arete('E', 'G')
        g.ajoute_arete('F', 'G')
        g.ajoute_arete('G', 'H')


        def DFSrec(g, traites, actuel):
            traites.append(actuel)
            for voisin in g.voisins(actuel):
                if voisin not in traites:
                    DFSrec(g, traites, voisin)
            return traites

        ```
    
        ```python
        >>> DFSrec(g, [], 'A')
        ['A', 'B', 'D', 'C', 'E', 'F', 'G', 'H']
        ```

    **Q3.** Reprendre les questions précédentes en changeant le sommet de départ.



#### 4.2.2 Parcours DFS itératif

Il «suffit» de remplacer la file du parcours BFS par une **pile**. Ainsi, on partira visiter le voisin tout juste ajouté à la *file d'attente* (qui porte maintenant mal son nom, puisque c'est devenu une pile).

!!! abstract "Parcours en profondeur itératif - DFS "
    ```python linenums='1'
    def DFS_iteratif(graphe, start):
        traites = []
        en_attente = [start]
        while en_attente != []:
            actuel = en_attente.pop()
            if actuel not in traites:
                voisins = g.voisins(actuel)[::-1]
                for voisin in voisins:
                    if voisin not in traites:
                        en_attente.append(voisin)
                traites.append(actuel)
        return traites
    ```

**Remarques :**

- À la ligne 7, on inverse l'ordre des voisins pour que ce code renvoie le même parcours quele parcours récursif (sinon c'est le dernier voisin ajouté qui sera dépilé). Cela n'est pas obligatoire : il n'y a pas «un seul» parcours DFS (tout comme il n'y a pas qu'un seul BFS). Ce qui les caractérise est la **méthode de découverte**, plus que l'implémentation proprement dite.

- Contrairement au BFS, il est possible d'empiler un sommet déjà découvert (on vérifie juste qu'il n'ait pas déjà été traité). Vous pouvez vous en apercevoir en écrivant l'état de la pile lors du parcours DFS itératif du graphe de l'exercice 6.
 
    
## Bilan

<div class="centre" markdown="span">
    <iframe 
        src="{{ page.canonical_url }}../activites/voc_graphes.html"
        width="900" height="800"
        id="quizz"
        title="Vocabulaire" 
        frameborder="0" 
        allow="autoplay; fullscreen; picture-in-picture">
    </iframe>
</div>
