---
author: Pierre Marquestaut, Gilles Lassus, Jean-Louis Thirot , Mireille Coilhac, Valérie Mousseaux
title: Arbres - Généralités
---

## I. Introduction

![arbres](img/arbre_envers.png){ width=60%; : .center }
> Source Gilles Lassus

!!! info "Arbre"

    Un arbre est une structure **hiérarchique** permettant de représenter de manière symbolique des informations structurées.

!!! example "L'arborescence d'un disque dur"

    Les systèmes Unix (MacOS ou GNU/Linux) organisent leur disque dur suivant l'arborescence ci-dessous :

    ![Unix](img/arbo-unix.gif){ width=40%; : .center }

!!! example "Un arbre généalogique des descendants ou des ascendants"

	```mermaid
	graph TD
	A(Vous)
	B(Père)
	C(Mère)
	F(Grand-père paternel)
	G(Grand-mère paternelle)
	D(Grand-père maternel)
	E(Grand-mère maternelle)
	A --- B
	A --- C
	B --- F
	B --- G
	C --- D
	C --- E
	```

!!! example "Organisation des matchs d'un tournoi de sport"

	```mermaid
	graph TD
	A(Vainqueur)
	B(Finaliste 1)
	C(Finaliste 2)
	D(Demi-finaliste 1)
	E(Demi-finaliste 2)
	F(Demi-finaliste 3)
	G(Demi-finaliste 4)
	H(Quart-finaliste 1)
	I(Quart-finaliste 2)
	J(Quart-finaliste 3)
	K(Quart-finaliste 4)
	L(Quart-finaliste 5)
	M(Quart-finaliste 6)
	N(Quart-finaliste 7)
	P(Quart-finaliste 8)
	A --- B
	A --- C
	B --- D
	B --- E
	C --- F
	C --- G
	D --- H
	D --- I
	E --- J
	E --- K
	F --- L
	F --- M
	G --- N
	G --- P
	```

## II. Terminologie

!!! warning "Attention"

    L'analogie avec les arbres réels peut s'avérer trompeuse. Les arbres - en informatique - sont le plus souvent représentés avec la racine en haut, puis les nœuds, et les feuilles en bas.

    ![racines envers](img/racine_envers.jpg){ width=10%; : .center }

!!! info "Un arbre"

	Un arbre est une structure hiérarchique de données, composée de nœuds.

	![terminologie](img/term.png){ width=30%; : .center }
	> Source Gilles Lassus

	* Chaque nœud a exactement un seul nœud père, à l'exception du nœud **racine** qui est le seul nœud à ne pas avoir de père. (oui, la racine d'une arbre est en haut)

	* Chaque nœud peut avoir un nombre quelconque de **fils**, dont il est le père.
	* Les nœuds qui n'ont pas de fils sont appelés les **feuilles**.
	* Chaque nœud est appelé par son **étiquette**.

!!! example "Exemple"

	```mermaid
	graph TD
	A(A)
	B(B)
	C(C)
	D(D)
	E(E)
	F(F)
	G(G)
	H(H)
	I(I)
	A --- B
	A --- C
	B --- D
	B --- E
	B --- F
	C --- G
	E --- H
	E --- I
	```

	* La racine est le nœud A.
	* Le nœud B possède 3 fils (les nœuds D, E et F), le noeud C possède un fils (le nœud G), le nœud F ne possède aucun fils.
	* Le nœud B a pour père le nœud A.
	* Les feuilles sont les nœuds D, H, I, F et G (ceux qui n'ont pas de fils).

## III. Caractéristiques

!!! info "Définitions"

	* La **taille** d'un arbre est le nombre de nœuds qu'il possède.

	* La **profondeur** d'un nœud ou d'une feuille d'un arbre est la **longueur du chemin** le plus court vers la racine .
		* La profondeur d’un nœud est égale à la profondeur de son père plus 1
		* Si un nœud est à une profondeur $p$, tous ses fils sont à une profondeur $p+1$

	* Il existe plusieurs méthode pour déterminer la profondeur d'un nœud :
		* La profondeur d'un nœud est le nombre d'arêtes entre la racine et ce nœud (c'est la convention retenue dans ce cours)
		* La profondeur d’un nœud est le nombre de nœuds du chemin qui va de la racine à ce nœud **sans compter la racine**
		* 🤿 on peut imaginer que la racine est à la surface de l'eau, donc à une profondeur 0, et que les nœuds sont à des profondeurs 1, 2, 3, etc.  
		Dans l'arbre précédent : profondeur de B = 1 - profondeur de I = 3 .

	* La hauteur d'un arbre est la profondeur de son nœud le plus profond.

	Dans l'arbre précédent : hauteur de l'arbre = 4

!!! danger "Attention"

    Attention : il faut bien repérer dans les définitions suivantes, si on compte les nœuds, ou si on regarde la longueur d'une branche.

	Il y a en effet un décalage de 1 entre ces deux nombres.
  

!!! danger "Conventions retenues dans ce cours"

	* ⚠️Un arbre ne contenant qu'un élément à une hauteur de 1.
	* La profondeur de la racine est 0

!!! danger "Autres définitions"

	Attention : On trouve aussi dans la littérature, que la profondeur de la racine est égale à 1, ce qui modifie la hauteur de l'arbre également puisqu'alors l'arbre réduit à la racine a pour hauteur 0 et l'arbre vide a pour hauteur -1. Les deux définitions se valent, il faut donc bien lire celle qui est donnée.

!!! example "Exemple"

	```mermaid
	graph TD
	A(A)
	B(B)
	C(C)
	D(D)
	E(E)
	F(F)
	G(G)
	H(H)
	I(I)
	A --- B
	A --- C
	B --- D
	B --- E
	B --- F
	C --- G
	E --- H
	E --- I
	```

	* La **taille** de l'arbre est égale à 9 (il possède 9 nœuds : 4 nœuds internes et 5 feuilles).
	* Le **nœud E a une profondeur** égale à 2 (le chemin A-B-E est de longueur 2).
	* La **hauteur de l'arbre** est égale à 4 car la branche A-B-E-H possède 4 nœuds (la profondeur maximale est égale à 3, c'est celle des nœuds les plus profonds : H et I. On a bien profondeur + 1 = hauteur).

!!! question "Exercice"

	<div class="centre" markdown="span">
		<iframe 
			src="{{ page.canonical_url }}../activites/voc arbre.html"
			width="900" height="400"
			id="quizz"
			title="Vocabulaire" 
			frameborder="0" 
			allow="autoplay; fullscreen; picture-in-picture">
		</iframe>
	</div>


## III. Arbres binaires

👉 Dans la suite, on ne s'intéressera qu'aux arbres dont les nœuds ont au plus deux fils.

Les arbres binaires sont des cas particuliers d'arbre : l'arbre du tournoi sportif et l'arbre "père, mère..." sont des arbres binaires, en revanche, l'arbre représentant la structure du système de fichier n'est pas un arbre binaire.

!!! info "Arbre binaire"

	Un arbre binaire est un arbre dont tous les nœuds ont au plus deux fils.


!!! example "Exemple"

	L'arbre vu dans le paragraphe précédent n'est pas binaire car le nœud B possède 3 fils. En revanche, l'arbre ci-dessous est lui un arbre binaire.

	```mermaid
	graph TD
	A(A)
	B(B)
	C(C)
	D(D)
	F(F)
	M( )
	G(G)
	E(E)
	H(H)
	I(I)
	L( )
	J(J)
	K(K)
	
	
	A --- B
	A --- C
	B --- D
	B --- F
	C --- M
	C --- G
	D --- E
	D --- H
	F --- I
	F --- L
	G --- J
	G --- K
	linkStyle 4 stroke-width:0px;
	linkStyle 9 stroke-width:0px;
	style L opacity:0;
	style M opacity:0;
	```

!!! info "Définition et vocabulaire spécifique aux arbres binaire"

	Les définitions vues précédemment pour des arbres quelconques restent valables pour les arbres binaires. Pour les arbres binaires :

	* chaque nœud possède deux sous-arbres, éventuellement vides, que l'on appelle sous-arbre gauche et sous-arbre droit.
	* les nœuds qui ne sont pas des feuilles peuvent avoir une fils gauche et/ou un fils droit.

	![sous_arbres.jpeg](img/sous_arbres.jpeg){ width=50%; : .center }

	Les sous-arbres gauche et droit de A sont eux-mêmes des arbres dont les racines sont respectivement B et C. B et C possèdent eux-même des sous-arbres gauche et droit.

	* le nœud C possède un sous-arbre gauche, qui est vide, et un sous-arbre droit qui est l'arbre dont la racine est G,
	* le nœud B possède un sous-arbre gauche, qui est l'arbre dont la racine est D, et un sous-arbre droit qui est l'arbre dont la racine est F.
	* et ainsi de suite.



!!! info "Fils gauche et fils droit"

	Il ne faut pas confondre fils gauche et fils droit, ainsi les arbres suivants ne sont pas les mêmes :

	![4 arbres](img/arbres_diff.png){ width=80%; : .center }


!!! info "Structure récursive"

	Il est aussi important de bien noter que l'on peut aussi voir les arbres comme des **structures récursives** : les fils d'un nœud sont des arbres (sous-arbre gauche et un sous-arbre droite dans le cas d'un arbre binaire), ces arbres sont eux mêmes constitués d'arbres...

	Chaque nœud d'un arbre binaire ne pouvant pas avoir plus de 2 fils, il est possible de séparer le «dessous» de chaque nœud en deux sous-arbres (éventuellement vides) : le **sous-arbre gauche** et le **sous-arbre droit**.

	![](img/sousarbres.png){: .center}


	- Les deux sous-arbres représentés ici sont les sous-arbres du nœud-racine T. 
	- Le nœud O admet comme sous-arbre gauche le nœud H et comme sous-arbre droit le nœud N.
	- Les feuilles P, H et N ont pour sous-arbre gauche et pour sous-arbre droit l'**arbre vide**.


???+ question "A vos crayons 😊" 

    Tracez tous les arbres binaires possibles avec 3 nœuds puis quelques-uns avec 4 nœuds.







!!! abstract "Relation entre hauteur et taille"

	On rencontre très souvent des arbres binaires dits **complets** parce qu'aucun des fils gauche ou droit n'est manquant.

	![](img/complet.png){: .center}


	**Taille d'un arbre complet de hauteur $h$ :**
	$1 + 2 + 2^2 + 2^3 + \dots + 2^{h-1} = 2^{h} - 1$

	*preuve* : ceci est la somme $S$ des $h$ premiers termes d'une suite géométrique de raison 2 et de premier terme 1, d'où $S= \frac{1-2^{h}}{1-2} = 2^{h} -1$.


	Un arbre complet de hauteur $h$ (en prenant la convention que l'arbre vide a pour hauteur 0) a donc une taille égale à $2^{h}-1$.

	**Remarque :** On en déduit une inégalité classique sur l'encadrement de la taille $t$ d'un arbre binaire (non nécessairement complet) de hauteur $h$ :

	$$h \leqslant t \leqslant 2^{h}-1$$

## IV. Parcours d'arbres
Les arbres étant une structure hiérarchique, leur utilisation implique la nécessité d'un **parcours** des valeurs stockées. Par exemple pour toutes les récupérer dans un certain ordre, ou bien pour en chercher une en particulier.  

Il existe plusieurs manières de parcourir un arbre.


### IV.1 Parcours en largeur d'abord (BFS)
*BFS : Breadth First Search*

!!! note "Méthode du parcours en largeur (BFS)" 
    Le parcours en largeur d'abord est un parcours étage par étage (de haut en bas) et de gauche à droite.

![](img/BFS.png){: .center}

L'ordre des lettres parcourues est donc T-Y-O-P-H-N.

Les trois parcours que nous allons voir maintenant sont des parcours en **profondeur d'abord**, ou **DFS** (*Depth First Search*). Ce qui signifie qu'un des deux sous-arbres sera totalement parcouru avant que l'exploration du deuxième ne commence. 


!!! question "Exercice"

	<div class="centre" markdown="span">
		<iframe 
			src="{{ page.canonical_url }}../activites/largeur.html"
			width="900" height="400"
			id="quizz"
			title="Vocabulaire" 
			frameborder="0" 
			allow="autoplay; fullscreen; picture-in-picture">
		</iframe>
	</div>

### IV.2 Parcours préfixe
Le parcours **préfixe** est un parcours **en profondeur d'abord**. 

!!! note "Méthode du parcours préfixe :heart:"
    (parfois aussi appelé *préordre*)

    - Chaque nœud est visité **avant** que ses fils le soient.
    - On part de la racine, puis on visite son fils gauche (et éventuellement le fils gauche de celui-ci, etc.) avant de remonter et de redescendre vers le fils droit.

![](img/prefixe.png){: .center}

L'ordre des lettres parcourues est donc T-Y-P-O-H-N.

!!! question "Exercice"

	<div class="centre" markdown="span">
		<iframe 
			src="{{ page.canonical_url }}../activites/prefixe.html"
			width="900" height="600"
			id="quizz"
			title="prefixe" 
			frameborder="0" 
			allow="autoplay; fullscreen; picture-in-picture">
		</iframe>
	</div>


### IV.3 Parcours infixe
Le parcours **infixe** est aussi un parcours en profondeur d'abord.

!!! note "Méthode du parcours infixe :heart:" 
    (parfois aussi appelé *en ordre*)

    - Chaque nœud est visité **après son fils gauche mais avant son fils droit**.
    - On part donc de la feuille la plus à gauche et on remonte par vagues sucessives. Un nœud ne peut pas être visité si son fils gauche ne l'a pas été.

![](img/infixe.png){: .center}

!!! question "Exercice"

	<div class="centre" markdown="span">
		<iframe 
			src="{{ page.canonical_url }}../activites/infixe.html"
			width="900" height="600"
			id="quizz"
			title="Vocabulaire" 
			frameborder="0" 
			allow="autoplay; fullscreen; picture-in-picture">
		</iframe>
	</div>


L'ordre des lettres parcourues est donc P-Y-T-H-O-N.

### IV.4 Parcours postfixe
Le parcours **postfixe** est aussi un parcours en profondeur d'abord.

!!! note "Méthode du parcours postfixe :heart:"
    (parfois aussi appelé **post-ordre** ou encore **suffixe**)

    - Chaque nœud est visité **après** ses fils le soient.
    - On part donc de la feuille la plus à gauche, et on ne remonte à un nœud père que si ses fils ont tous été visités. 

![](img/postfixe.png){: .center}

L'ordre des lettres parcourues est donc P-Y-H-N-O-T.




!!! question "Exercice 5"

	<div class="centre" markdown="span">
		<iframe 
			src="{{ page.canonical_url }}../activites/suffixe.html"
			width="900" height="600"
			id="quizz"
			title="Vocabulaire" 
			frameborder="0" 
			allow="autoplay; fullscreen; picture-in-picture">
		</iframe>
	</div>


### IV.5 Comment ne pas se mélanger entre le pré / in / suf fixe ?

- *pré* veut dire *avant*
- *in* veut dire *au milieu*
- *post* veut dire *après*

Ces trois mots-clés parlent de la place du **père** par rapport à ses fils. 
Ensuite, il faut toujours se souvenir qu'on traite le fils gauche avant le fils droit.

- préfixe : le père doit être le premier par rapport à ses fils.
- infixe : le père doit être entre son fils gauche (traité en premier) et son fils droit.
- postfixe : le père ne doit être traité que quand ses deux fils (gauche d'abord, droite ensuite) l'ont été.

Un parcours préfixe commencera toujours par la racine, alors qu'un parcours postfixe finira toujours par la racine. Dans un parcours infixe, la racine sera «au milieu» (pas nécessairement parfaitement).



!!! example "{{ exercice() }}"
    ![](img/exo_parcours.png){: .center}
    === "Énoncé"
        Donner le rendu de chaque parcours :

        1. Parcours en largeur 
        2. Parcours préfixe
        3. Parcours infixe
        4. Parcours postfixe
    === "Corr. largeur"
        largeur : 1 2 3 4 5 6 7 8 9
    === "Corr. préfixe"
        préfixe : 1 2 4 5 7 8 3 6 9
    === "Corr. infixe"
        infixe : 4 2 7 5 8 1 3 9 6
    === "Corr. postfixe"
        postfixe : 4 7 8 5 2 9 6 3 1


!!! example "{{ exercice() }}"
    ![](img/exo_2.png){: .center}
    === "Énoncé"
        Donner le rendu de chaque parcours :

        1. Parcours en largeur 
        2. Parcours préfixe
        3. Parcours infixe
        4. Parcours postfixe
    === "Corr. largeur"
        largeur : 9 8 7 6 2 5 1 4 3
    === "Corr. préfixe"
        préfixe : 9 8 6 2 1 7 5 4 3
    === "Corr. infixe"
        infixe : 6 8 1 2 9 7 4 5 3
    === "Corr. postfixe"
        postfixe : 6 1 2 8 4 3 5 7 9




## V. Implémentations d'un arbre binaire
### V.1 En utilisant la Programmation Orientée Objet
Le but est d'obtenir l'interface ci-dessous.

Il est à remarquer que ce que nous allons appeler «Arbre» est en fait un nœud et ses deux fils gauche et droit.

!!! abstract "interface souhaitée"
    ```python
    >>> a = Arbre(4) # pour créer l'arbre dont le nœud a pour valeur 4,
                # et dont les sous-arbres gauche et droit sont None
    >>> a.left = Arbre(3) # pour donner la valeur 3 au nœud du sous-arbre gauche de a
    >>> a.right = Arbre(1) # pour donner la valeur 1 au nœud du sous-arbre droit de a
    >>> a.right.data # pour accéder à la valeur du fils droit de a
    ```


!!! example "{{ exercice() }}"
    === "Énoncé"
        Dessinez l'arbre créé par les instructions suivantes :
        ```python
        >>> a = Arbre(4)
        >>> a.left = Arbre(3)
        >>> a.right = Arbre(1)
        >>> a.right.left = Arbre(2)
        >>> a.right.right = Arbre(7)
        >>> a.left.left = Arbre(6)
        >>> a.right.right.left = Arbre(9)
        ```
    === "Correction"
		```mermaid
		graph TD
		A(4)
		B(3)
		C(1)
		D(2)
		E(7)
		F(6)
		G(9)
		A --- C
		A --- B	
		C --- D
		C --- E
		B --- F
		E --- G
		```



**:star: Implémentation :star:**

!!! abstract "Principe :" 
	Nous allons créer une classe ```Arbre```, qui contiendra 3 attributs : 

	- ```data``` : la valeur du nœud (de type ```Int```)
	- ```left``` : le sous-arbre gauche (de type ```Arbre```)
	- ```right``` : le sous-arbre droit (de type ```Arbre```).

	Par défaut, les attributs ```left ``` et ```right``` seront à ```None```, qui représentera l'arbre vide (ce qui n'est pas très rigoureux, car ```None``` n'est pas de type ```Arbre```...).

!!! note "Définition récursive"
	On peut constater que la définition de la classe `Arbre` est **récursive**, puisqu'elle possède elle-même des attributs de type `Arbre`.

!!! note "Encapsulation ou pas ??? :"

	Afin de respecter le paradigme de la Programmation Orientée Objet, nous devrions jouer totalement le jeu de l'**encapsulation** en nous refusant d'accéder directement aux attributs.

	Pour cela  il faut construire des méthodes permettant d'accéder à ces attributs (avec des **getters**, ou **accesseurs** en français) ou de les modifier (avec des **setters**, ou **mutateurs** en français) .

	Néanmoins, une version sans encapsulation avec accès direct aux attributs (comme dans l'exemple ci-dessous) est beaucoup plus simple et rapide.



!!! note "Classe `Arbre` sans encapsulation"
    ```python linenums='1'
    class Arbre:
        def __init__(self, data):
            self.data = data
            self.left = None
            self.right = None
    ```



```python
a = Arbre(4)
a.left = Arbre(3)
a.right = Arbre(1)
a.right.left = Arbre(2)
a.right.right = Arbre(7)
a.left.left = Arbre(6)
a.right.right.left = Arbre(9)
```


```python
>>> a
   <__main__.Arbre at 0x7f0100361f40>
```


```python
>>> a.right.left.data
   2
```



### 3.2 Implémentation à partir de tuples imbriqués



!!! note "`Arbre` sous forme de tuples imbriqués :heart:"
    Un arbre peut se représenter par le tuple ```(valeur, sous-arbre gauche, sous-arbre droit)```.
    L'arbre ci-dessous :
    ![](img/imp_tuple.png){: .center}
    est représenté par le tuple :


    ```python
    >>> a = (2, (8, (6,(),()), (9,(),())), (1, (7, (),()), ()))
    ```

Le sous-arbre gauche est alors ```a[1]``` et le sous-arbre droit est ```a[2]```.


```python
>>> a[1]
(8, (6, (), ()), (9, (), ()))
>>> a[2]
(1, (7, (), ()), ())
```

!!! example "{{ exercice() }}"
    ![](img/carac3.png){: .center}
    === "Énoncé"
        Écrire le tuple représentant l'arbre ci-dessous.       
        
    === "Correction"
        ```python
        a = (T,(Y,(P,(),()),()),(O,(H,(),()),(N,(),())))
        ```



### 3.3 Implémentation à partir d'une «simple» liste
De manière plus surprenante, il existe une méthode pour implémenter un arbre binaire (qui est une structure hiérarchique) avec une liste (qui est une structure linéaire). 
Ceci peut se faire par le biais d'une astuce sur les indices :

**Les fils du nœud d'indice i sont placés aux indice 2i+1 et 2i+2**.

Cette méthode est connue sous le nom de «méthode d'Eytzinger», et utilisée notamment en [généalogie](https://fr.wikipedia.org/wiki/Num%C3%A9rotation_de_Sosa-Stradonitz) pour numéroter facilement les individus d'un arbre généalogique.



**Exemple :**

![](img/eytzinger.png){: .center}


Pour comprendre facilement la numérotation, il suffit de s'imaginer l'arbre complet (en rajoutant les fils vides) et de faire une numérotation en largeur, niveau par niveau :

![](img/eytzinger2.png){: .center}



!!! example "{{ exercice() }}"
    === "Énoncé"
        Si on note Δ le sous-arbre vide, dessiner l'arbre représenté par la liste :
        ```python
        a = [3, 4, Δ, 7, 5]
        ```       
        
    === "Correction"
        ![correction](img/corrtuple.png){: .center}


**Remarque :** parfois (comme dans le sujet 0...) la racine de l'arbre est placée à l'indice 1. Dans ce cas, les fils du nœud d'indice i sont placés aux indice 2i et 2i+1.

## 4. Utilisation de l'implémentation : parcours, taille...

Dans toute la suite, sauf mention contraire, on utilisera l'implémentation en Programmation Orientée Objet, en version sans encapsulation (la plus simple).
Nous allons créer des fonctions renvoyant les différents parcours d'un arbre, ou encore sa taille, sa hauteur, son nombre de feuilles... Toutes ses fonctions exploiteront la structure **récursive** d'un arbre.


**Rappel de l'implémentation :**

```python linenums='1'
class Arbre:
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None

```

### 4.1 Parcours préfixe, infixe, postfixe

#### 4.1.1 Parcours préfixe


<iframe src="https://www.codepuzzle.io/PYJMVW" width="100%" height="400" frameborder="0"></iframe>

```python linenums='1'
print(arbre.data, end = '-')
```


Exemple avec l'arbre 
![](img/exo_2.png){: .center}


```python linenums='1'
a = Arbre(9)
a.left = Arbre(8)
a.right = Arbre(7)
a.left.left = Arbre(6)
a.left.right = Arbre(2)
a.right.right = Arbre(5)
a.left.right.left = Arbre(1)
a.right.right.left = Arbre(4)
a.right.right.right = Arbre(3)
```


```python
>>> prefixe(a)
9-8-6-2-1-7-5-4-3-
```

#### 4.1.2 Parcours infixe

<iframe src="https://www.codepuzzle.io/IPHVRFY" width="100%" height="400" frameborder="0"></iframe>


```python
>>> infixe(a)
6-8-1-2-9-7-4-5-3-
```

#### 4.1.3 Parcours suffixe

<iframe src="https://www.codepuzzle.io/IPMJZQY" width="100%" height="400" frameborder="0"></iframe>



```python
>>> postfixe(a)
6-1-2-8-4-3-5-7-9-
```

!!! aide "Pause vidéo" 
    - Regardez et appréciez [cette vidéo](https://youtu.be/OTfp2_SwxHk){. target="_blank"}
    - À l'aide de la vidéo, codez le parcours infixe en itératif.  
    ??? tip "solution"
        ```python linenums='1'
        def infixe(arbre):
            parcours = []
            pile = []

            current = arbre

            while pile != [] or current is not None:
                if current is not None:
                    pile.append(current)
                    current = current.left
                else:
                    current = pile.pop()
                    parcours.append(current.data)
                    current = current.right

            return parcours
        ```

### 4.2 Calcul de la taille d'un arbre
Rappel : la taille d'un arbre est le nombre de ses nœuds.

!!! note "Taille d'un arbre :heart:"

    ```python
    def taille(arbre):
        if arbre is None:
            return 0
        else:
            return 1 + taille(arbre.left) + taille(arbre.right)
    ```



Exemple avec l'arbre 
![](img/exo_2.png){: .center}


```python linenums='1'
a = Arbre(9)
a.left = Arbre(8)
a.right = Arbre(7)
a.left.left = Arbre(6)
a.left.right = Arbre(2)
a.right.right = Arbre(5)
a.left.right.left = Arbre(1)
a.right.right.left = Arbre(4)
a.right.right.right = Arbre(3)
```

```python
>>> taille(a)
9
```

### 4.3 Calcul de la hauteur d'un arbre
Rappel : on prendra comme convention que l'arbre vide a pour hauteur 0.

!!! note "Hauteur d'un arbre :heart:"

    ```python
    def hauteur(arbre):
        if arbre is None:
            return 0
        else:
            return 1 + max(hauteur(arbre.left), hauteur(arbre.right))
    ```



```python
>>> hauteur(a)
4
```


### 4.4 Calcul du nombre de feuilles d'un arbre
Rappel : une feuille est un nœud d'arité 0, autrement dit sans fils gauche ni fils droit.

!!! note "Nombre de feuilles d'un arbre :heart:"

    ```python
    def nb_feuilles(arbre):
        if arbre is None:
            return 0
        if (arbre.left is None) and (arbre.right is None):
            return 1
        return nb_feuilles(arbre.left) +  nb_feuilles(arbre.right)
    ```


```python
>>> nb_feuilles(a)
4
```



### 4.5 Recherche d'une valeur dans un arbre
On renverra ```True``` ou ```False``` en fonction de la présence ou non de la valeur dans l'arbre.

!!! note "Recherche d'une valeur dans un arbre :heart:"
    
    ```python
    def recherche(arbre, valeur):
        if arbre is None:
            return False
        if arbre.data ==  valeur:
            return True
        return recherche(arbre.left, valeur) or recherche(arbre.right, valeur)
    ```
    


```python
>>> recherche(a, 2)
True
>>> recherche(a, 45)
False
```

### 4.6 Parcours en largeur
Le parcours en largeur (BFS) est le plus simple à faire visuellement : mais il est plus difficile à coder que les parcours préfixe, infixe, postfixe.  
Il est nécessaire d'utiliser une **file**  :

- On place l'arbre dans la file.
- Tant que la file n'est pas vide, on procède comme suit :
    - On défile, donc on récupère l'arbre situé en haut de la file.  
    - Si cet arbre n'est pas vide :
        - On garde son étiquette.
        - On enfile son sous-arbre gauche, puis son sous-arbre droit.

![](img/parcoursBFS.png){: .center}

On importera l'objet ```Queue()``` du module ```queue``` de Python, qui permet de  :

- créer une file vide avec ```file = Queue()```
- défiler un élément par ```file.get()```
- enfiler l'élément ```a``` par ```file.put(a)```
- savoir si la file est vide par le booléen ```file.empty()```


```python
# arbre-test
# ne pas oublier de remonter plus haut dans le document pour relancer la classe Arbre
a = Arbre(8)
a.left = Arbre(4)
a.right = Arbre(5)
a.left.left = Arbre(2)
a.left.right = Arbre(1)
a.right.right = Arbre(3)
```

!!! note "Parcours en largeur (BFS) "
    ```python
    from queue import Queue

    def BFS(arbre):        
        file = Queue()
        file.put(arbre)
        sol = []
        while not file.empty():
            a = file.get()
            if a is not None :
                sol.append(a.data)
                file.put(a.left)
                file.put(a.right)
        return sol
    ```


```python
>>> BFS(a)
[8, 4, 5, 2, 1, 3]
```

