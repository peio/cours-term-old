---
author: Mireille Coilhac, Valérie Mousseaux, Jean-Louis Thirot, Pierre Marquestaut
title: Programmation Orienté Objet
---

# Programmation Orienté Objet

## 1. Introduction

!!! tips "Contexte"

	Imaginons une application pour gérer les entrées et sorties d'un parking souterrain.

	Cette application devra comptabiliser le nombre de voitures, calculer la tarification et gérer les barrières d'entrées et de sortie.

	![parking](img/underground.jpg){ width=50% }

	Pour réaliser cette application, il est nécessaire d'avoir plusieurs variables :

	* le nombre maximum de places,
	* le nombre de places occupées,
	* la liste des immatriculations des voitures garées avec l'horodatage d'entrée,
	* la liste des abonnés,
	* si le parking est plein.

	On aura également besoin de plusieurs fonctions, principalement :

	* une fonction qui gére l'enregistrement de la voiture en entrée,
	* une qui s'assure du paiement,
	* une qui s'assure que la voiture peut sortir.

	Ces différentes fonctions seront amener à manipuler ces différentes variables communes.

!!! problem "Problématique"
	Peut-on regrouper dans une même structure de données les caractéristiques d'un élément et les fonctions qui lui sont associées ?

## 2. Définition

!!! abstract "Paradigmes"
	Il existe différentes manières de voir la programmation, on parle de différents **paradigmes** de programmation. 
	
	L'un d'eux est le **paradigme objet**.

!!! info "La programmation orienté objet"
	Dans le paradigme ou Programmation Orienté Objets (POO) on associe à chaque objet physique un **objet** logiciel.

!!! example "Exemple"
	Notre parking dispose de deux barrières automatiques.

	Notre application devra donc disposer d'un objet `Parking` et de deux objets `Barriere`.


!!! abstract "Méthodes et attributs"
	Un objet est une structure logicielle qui contient :

	* des **attributs** : les caractéristiques de l'objet,
	* des **méthodes** : les actions qu’il est capable de réaliser.
	
	On manipule les objets par modification de ses attributs ou par appel à ses méthodes .

!!! example "Méthodes et attributs"
	Si on prend un objet `barriere`, il aura deux méthodes `ouvre` et `ferme`.

	Dans notre application, il serait intéressant pour un objet `voiture` d'avoir les attributs `immatriculation` et `horodatage`.

!!! info "La notation pointée"

	Les attributs et méthodes d'une instance de classe sont accessibles en utilisant la notation pointée : 

	* `objet.attribut`
	* `objet.methode(arguments)`

!!! example "Exemple"
	```python
	parking_centreville.place_occupees -= 1
	barriere_sortie.ouvrir()
	```

!!! example "Exemple de **classe**"

	Reprenons l'exemple du Parking. 
	
	Nous avons vu que la gestion de ce parking nécessite un certains nombre de variables et de fonctions.

	Ses attributs et méthodes peuvent être réunis dans ce qu'on appelle une classe qui est donc un modèle décrivant un objet.
	
	On peut la schématiser ainsi :

	```mermaid
		classDiagram
    	class Parking{
      		int places_occupees
      		int places_max
      		list voiture_entrees
      		bool est_complet
      		enregistrer_voiture()
      		fait_payer()
      		fait_sortir()
    	}
    ```

!!! abstract "Une classe "

	Une **classe** permet de définir un ensemble d’objets qui ont des caractéristiques communes. 
	
	C'est une définition générique d'objets.

!!! abstract "Les instances"

	A partir d'une classe, il est possible de créer autant d'objets que nécessaires.

	On dit que chaque objet sera une **instance** de la classe.

!!! example "Instance de voiture"

	De la même façon, on peut créer une classe Voiture :

	```mermaid
	classDiagram
	class Voiture{
		str immatriculation
		str horodatage
		bool abonné
	}
    ```

	A partir de cette classe, on peut créer une instance pour chaque voiture qui entre dans le parking.



## 3. Activités


!!! question "Vocabulaire"

	<div class="centre" markdown="span">
		<iframe 
			src="{{ page.canonical_url }}../activites/voc POO.html"
			width="100%" height="320"
			id="voc"
			title="Vocabulaire" 
			frameborder="0" 
			allow="autoplay; fullscreen; picture-in-picture">
		</iframe>
	</div>

!!! question "TP"
	[Syntaxe de la POO](https://codex.forge.apps.education.fr/exercices/syntaxe_poo/){ .md-button target="_blank" rel="noopener" }

## 4. Classes et objets en Python


!!! note "Classe et type"
	Définir une classe revient à créer un nouveau type.




??? tips "En Python, tout est objet !"

    Toutes les variables que l'on crée avec Python sont considérées comme des objets, et tous les types (int, float, str...) comme des classes.

	Tester le code suivant dans la console :

	```pycon
    >>> a = 7.3
    >>> type(a)
    ```

	{{ terminal() }}

	La variable `a` est bien une instance de la classe `float`.

!!! abstract "Le mot clé `class`"

	En Python, on utilise le mot clé `class` pour définir une classe qui devient alors un nouveau type abstrait de données. 
	
	Par convention, le nom d'une classe commence par une majuscule.

!!! example "Exemple"

	```python
	class Barriere:
			...
	```

!!! abstract "Le mot clé `self`"

	En Python, le mot clé `self` désigne l'objet à l'intérieur de la définition de la classe.

!!! example "Exemple"

	Pour modifier un attribut dans la définition de la classe, on écrira :

	```python
	self.horodatage = "11/03/2023 14:32:00"
	```
!!! warning "méthode et `self`"
	En Python, toute méthode doit avoir `self` comme premier paramètre.

	```python
	class Barriere:
	    def ouvrir(self):
			...
	```

	Par contre, il n'apparait pas dans les arguments lors de l'appel de la méthode :

	```python
	barriere_sortie = Barriere()
	barriere_sortie.ouvrir()
	```

!!! abstract "La méthode `__init__` "

	 En Python, la méthode `__init__` est le **constructeur** de la classe. Elle est utilisée à la création des objets de la classe et initialise les valeurs des attributs de l’objet.

!!! example "Constructeur de la classe Parking"

	On peut créer un constructeur à la classe `Parking` :

	```python
	class Parking:
	    def __init__(self, nb_max):
			self.places_max = nb_max
	```

	On peut instancier la classe.
	```python
	parking_centreville = Parking(370)
	```
	Dans ce cas, on a créé un objet de la classe `Parking` dont l'attribut `places_max` est initialisé à la valeur $370$.




## 5. Exercices


!!! question "Syntaxe de la classe"

	<div class="centre" markdown="span">
		<iframe 
			src="{{ page.canonical_url }}../activites/classe POO.html"
			width="900" height="300"
			id="classe"
			title="Classe" 
			frameborder="0" 
			allow="autoplay; fullscreen; picture-in-picture">
		</iframe>
	</div>

	

!!! question "TP"
	[Création d'une classe](https://codex.forge.apps.education.fr/exercices/poo_chien/){ .md-button target="_blank" rel="noopener" }
