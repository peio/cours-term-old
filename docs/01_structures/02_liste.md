---
author: Pierre Marquestaut
title: Structures linéaires
---
# Les structures linéaires

## Les tableaux

En informatique, un tableau est structure de données avec les caractéristiques suivantes :

* un tableau est une **structure de données linéaire**. Cela signifie qu'il contient différents éléments, rangés dans un certain ordre.
* un tableau est de **longueur fixe**. Une fois créé, il est impossible d'ajouter ou de supprimer des valeurs.
* les éléments d'un tableau sont tous du **même type**. Un tableau ne peut donc pas contenir un nombre entier suivi d'une chaîne de caractères.

Connaissant l'indice d'un élément dans un tableau, il est possible de récupérer sa valeur en utilisant la notation #!py tableau[indice]. Par exemple :

```pycon
>>> sports = ['rugby', 'handball', 'voleyball', 'ultimate']
>>> sports[0]
'rugby'
>>> sports[3] = 'football'
```

!!! note "Coût des opérations"

    Dans les langages de programmation tels que C, un tableau est stocké en machine sous forme de cellules contigües. En Python, le stockage est légèrement plus complexe mais repose sur le même principe.

    Lors d'une opéaration d'accès ou de modification d'un élément, l'adresse de l'élément est calculée en fonction de son indice. Ces opérations d'accès et de modifications d'un élément reviennent alors à la lecture ou à l'écriture d'une zone mémoire et se font ainsi à coût constant $O(1)$.

!!! warning "Taille fixe"

    La taille d'un tableau est fixé lors de sa création. Il n'est donc pas possible de rajouter ou d'enlever des éléments. 
    Si l'on souhaite pouvoir ajouter ou enlever des éléments durant l'exécution du programme, il faudra utiliser d'autres types de structure de données.


## Listes, piles et files

### Listes

Une liste est une structure de données dans laquelle on peut ajouter ou supprimer des éléments, peu importe leur position.
On considère qu'une liste à deux extrémités, appelées **tête** et **queue**

![liste](img/liste.svg){ width=25% }

Par exemple, à partir d'une liste à 3 éléments on peut en rajouter un quatrième en queue de la liste.
![liste](img/ajout.svg){ width=25% }

Les opérations associées (ou **primitives**) d'une liste sont les suivantes :

* **crée_liste_vide** : action de créer une liste vide
* **insère** : action d'ajouter un élément à une position donnée
* **retire** : action de retirer un élément à une position donnée
* **est_liste vide** : détermine si la liste est vide

!!! note "primitives supplémentaires"

    Il s'agit là des opérations les plus courantes.
    On peut parfois en rajouter d'autres, comme par exemple **recherche_élément**.

En Python, les listes peuvent être créées par le type `#!py list`. Ce type dispose en effet de méthodes pour ajouter ou supprimer des éléments.

* Il est possible d'initialiser une liste vide

```python
>>> liste_vide = []
```

* La méthode `append` permet d'ajouter un élément en queue de liste.

```python
>>> nouvelle_liste = [1, 2, 3]
>>> nouvelle_liste.append(4)
>>> nouvelle_liste
[1, 2, 3, 4]
```

* La méthode `pop` retire un élément de la liste et le renvoie. Sans paramètre, elle retire par défaut l'élément en queue de la liste.

```python
>>> nouvelle_liste = [1, 2, 3, 4, 5]
>>> nouvelle_liste.pop()
5
>>> nouvelle_liste.pop(0)
1
>>> nouvelle_liste
[2, 3, 4]
```
!!! note "autres méthodes"
    Le type `list` dispose d'autres méthodes pour manipuler les éléments d'une liste mais ces méthodes sortent du cadre de ce cours.

!!! faq "Exercice"
    [Implémentation avec le type list](){ .md-button }


### les piles


Les piles sont un cas particuliers des listes dont les éléments ne peuvent être ajoutés ou retirés qu'à partir d'une même extrémité de la pile,, que l'on appele généralement le **sommet** de la pile. (Contrairement aux listes et aux files, on représentera le plus souvent une pile de façon verticale)

![liste](img/pile.svg){ width=25% }

Ce fonctionnement est appelé **LIFO** : Last In, First Out.

C'est le principe de la pile d'assiettes : on pose les assiettes sur les assiettes précédentes , et quand on prend une assiette, on la prend sur le dessus.

Les **primitives** d'une pile sont les suivantes :

* **empile** : action d'ajouter un élément en haut de la pile
* **dépile** : action de retirer un élément du haut de la pile
* **est_pile vide** : détermine si la pile est vide

???+ question "Contenu d'une pile"

    Une pile contient les éléments suivants : `#!py nombres = [8, 7, 2, 10, 3, 1]`,  avec `#!py 1` le sommet de la pile.


    ```python
    depile()
    empile(5)
    empile(4)
    depile()
    empile(9)
    ```

    === "Que contient la pile après les opérations suivantes ? (Cocher la réponse correcte)"
        
        - [ ] `#!py [8, 7, 2, 10, 3, 5, 9]`
        - [ ] `#!py [2, 10, 3, 1, 5, 4, 9]`
        - [ ] `#!py [8, 7, 2, 10, 3, 9, 5]`
        - [ ] `#!py [8, 7, 2, 10, 3, 1, 5, 4, 9]`

    === "Solution"
        
        - :white_check_mark: on enlève la valeur `#!py 1`, puis on empile les valeurs `#!py 5` et `#!py 4`, puis on enlève la valeur `#!py 4` et on termine par ajouter la valeur `#!py 9`.
        - :x: le fonctionnement de la pile se fait en mode LIFO, où la dernière valeur ajoutée est la première à être enlevée.
        - :x: les valeurs doivent se trouver dans l'ordre où elles ont été ajoutées.
        - :x: l'action de dépiler entraine la suppression de la valeur qui se trouve au sommet de la pile.



En Python, les piles peuvent être créées par le type `list`.
Dans ce cas, on limite l'interface à la méthode `#!py append` pour empiler un élément et `#!py pop` pour dépiler un élément.

!!! faq "Exercice"
    [Implémentation avec le type list](){ .md-button }




### les files


Les files sont des listes particulières dont les éléments ne peuvent être ajoutés qu'en queue, et retirés qu'à partir de la tête.


![files](img/file.svg){ width=25% }

Ce fonctionnement est appelé **FIFO** : First In, First Out.

C'est le principe des files d'attente : les personnes entrent dans une file et en ressortent dans le même ordre dans lequel elles sont rentrées.

Les **primitives** d'une pile sont les suivantes :

* **enfile** : action d'ajouter un élément en tête de la file
* **défile** : action de retirer un élément de la queue de la file
* **est_file vide** : détermine si la file est vide

???+ question "Contenu d'une pile"

    Une file contient les éléments suivants : `#!py nombres = [8, 7, 2, 10, 3, 1]`,  avec `#!py 8` la tête de la file et `#!py 1` sa queue.

    ```python
    defile()
    enfile(5)
    enfile(4)
    defile() 
    enfile(9)
    ```

    === "Que contient la file après les opérations suivantes ? (Cocher la bonne réponse)"
        
        - [ ] `#!py [8, 7, 2, 10, 3, 5, 9]`
        - [ ] `#!py [2, 10, 3, 1, 5, 4, 9]`
        - [ ] `#!py [2, 10, 3, 9, 5, 4]`
        - [ ] `#!py [8, 7, 2, 10, 3, 1, 5, 4, 9]`

    === "Solution"

        - :x: le fonctionnement de la file se fait en mode FIFO, où la première valeur ajoutée est la première à être enlevée.
        - :white_check_mark: on enlève la valeur `#!py 8`, puis on enfile les valeurs `#!py 5` et `#!py 4`, puis on enlève la valeur `#!py 7` et on termine par ajouter la valeur `#!py 9`.
        - :x: les valeurs doivent se trouver dans l'ordre où elles ont été ajoutées.
        - :x: l'action de défiler entraine la suppression de la valeur qui se trouve au sommet de la pile.


En Python, les files peuvent être créées par le type `list`.
Dans ce cas, on limite l'interface à la méthode `#!py append` pour enfiler un élément et `#!py pop(0)` pour défiler un élément.

!!! faq "Exercice"
    [Implémentation avec le type list](){ .md-button }



## Interface et implémentation

!!! note "Remarque"

    A noter que les opérations réalisées par le type `list`ne sont pas toutes à coût constant. 

Le type `list` du langage Python est facile d'utilisation. Cependant, les opérations qu'il propose se révèlent généralement couteuses, car consistent en interne à recréer un tableau à chaque ajout ou suppression d'élément : on parle alors plutôt de **tableau dynamique**. Ce type est donc adapté à des programmes simples, mais peut devenir vite limité lorsque le nombre d'éléments manipulés devient important.


Une **structure de données abstraite** est une spécification d'un ensemble de données et de l'ensemble des opérateurs associés.
Une même structure abstraite peut donc être implémentée par des structures concrètes différentes. Il est donc nécessaire de choisir l'implémentation la plus adaptée à la situation.


L’implémentation est la réalisation concrète de la structure de données, par l’utilisation de types et structures déjà existantes.
Exemple : Implémentation d'une structure de file à l'aide d'un tableau


L'interface d'une structure de données abstraite est l'ensemble des opérateurs nécessaires à la manipulation de cette structure, appelés **primitives**.

Exemple : primitives de la structure de File
`!py cree_file()`
`!py enfile()`
`!py défile()`
`!py est_vide()`

!!! note "Remarque"

    L’interface est indépendante de l’implémentation. L’implémentation peut être modifiée mais l’utilisation de la structure de données reste identique.


???+ question "type de base en python"


    === "Parmi ces types, quels sont ceux qui peuvent être considérés comme une structure linéaire ?"
        
        - [ ] le type `list`
        - [ ] le type `dict`
        - [ ] le type `tuple`
        - [ ] le type `int`

    === "Solution"
        
        - :white_check_mark: il s'agit bien d'une structure linéaire (qui sera détaillée au chapitre suivant !)
        - :x: il ne s'agit pas d'une structure linéaire car les éléments ne sont pas ordonnées.
        - :white_check_mark:  il s'agit bien d'une structure linéaire, mais qui ne permet pas de modifier les valeurs.
        - :x: il ne s'agit pas d'une structure linéaire car la variable de ce type ne contient qu'une seule valeur.

!!! note "Programmation Orientée Objets"
Objets
Le paradigme de Programmation Orientée Objets est Objets souvent utilisé pour créer des structures de données. Il permet d'encapsuler dans une classe à la fois l'implémentation et l'interface de la structure. Le programmeur peut alors utiliser la structure de données au travers de son interface, sans avoir besoin de connaître son implémentation concrète.

Le diagramme ci-dessous présente la classe Pile. La connaissance de ses méthodes est suffisante pour pouvoir utiliser cette classe, et ce quelle que soit la manière dont la classe gère les données en mémoire.

```mermaid
classDiagram
    class Pile
    Pile : +list données 
    Pile : empile
    Pile : depile
    Pile : est_vide
```

## Implémentations alternatives

On a pu voir que les méthodes du type `list` peuvent être à coût non linéaire. Ainsi on privilégiera souvent des implémentations plus efficaces, surtout pour des algorithmes qui nécessitent un nombre important d'ajouts et de retraits d'éléments.

### Pile avec un tableau

On peut implémenter une pile grâce à un tableau à taille fixe. Dans ce cas, le nombre d'éléments de la pile peut varier, mais ne pourra pas dépasser une taille maximale, qui correspond à la taille du tableau.

On stocke dans une variable l'indice maximum. Pour une pile vide, cet indice vaut 0.

* Quand on empile une valeur, on stocke l'élément au niveau de l'indice, puis on incrémente l'indice.

![files](img/pile_tab.svg){ width=50% }

* Quand on dépile une valeur, on décrémente l'indice et on renvoie la valeur correspondant à l'indice.

!!! bug "Stack overflow"

    L'erreur de débordemant de pile est une erreur courante lors de l'exécution d'un programme. En début d'exécution, une zone mémoire est allouée à la pile d'exécution. Une erreur se produit si le programme tente d'écrire à l'extérieur de cette zone, risquant d'écraser des informations importantes.  



### File avec deux piles

Il est également possible d'implémenter une file à l'aide de deux piles.

Pour enfiler un nouvel élément dans la file, on l'empile dans la *pile d'entrée*.

![enfiler](file2pile1.svg){ width=50% }

Pour défiler un élément de la file, deux cas se présentent :

* La pile de sortie n'est pas vide : on la dépile.

![enfiler](img/file2pile2.svg){ width=50% }

* La pile de sortie est vide : on dépile les éléments de la pile d'entrée en les empilant dans la pile de sortie jusqu'à ce que la pile d'entrée soit vide, puis on dépile un élément de la pile de sortie.

![enfiler](img/file2pile3.svg){ width=50% }

On constate alors que la valeur défilée est la première qui a été enfilée, ce qui correspond bien à un fonctionnement FIFO.

### Liste chaînée

En informatique, quand on parle de listes, on pense souvent aux **listes chainées**, qui est la manière la plus répandue d'implémenter cette structure, quel que soit le langage.

Une liste chainée est composée de **maillon**, chaque maillon possédant une valeur ainsi qu'un lien vers le maillon suivant.

![liste](img/maillon.svg){ width=20% }



Une liste chainée est donc une succession de maillons :

![liste](img/maillon2.svg){ width=50% }

Un maillon peut être implémenté par une classe qui possède deux attributs : une valeur (qui dans notre exemple est de type entier) et le `prochain_maillon` qui est lui-même de type `Maillon`. 

```mermaid
classDiagram
    class Maillon
    Maillon : +int valeur
    Maillon : +Maillon prochain_maillon
```
!!! note 
    On notera ici la définition **recursive** de cette classe.

!!! faq "Exercice"
    [Implémentation avec une liste chainée](https://e-nsi.gitlab.io/pratique/N2/770-file_avec_liste/sujet/){ .md-button }

## Approches récursives

Les différentes structures de listes, piles ou files peuvent être définies récursivement.

### Approche récursive d'une pile

Un pile peut être définie récursivement par :

* soit une pile est vide
* soit une pile est composée d'un sommet et du reste de la pile, le reste de la pile étant lui-même une pile

En utilisant des tuples, une pile vide est représentée par un tuple vide :

`#!py pile_vide = ()`

Une pile ne contenant que l'élément 5 sera donc un tuple composé de l'entier 5 et d'un tuple vide :

`#!py pile_avec_un_element = (5, ())`

L'action d'empiler revient à créer un tuple contenant la nouvelle valeur et l'ancienne pile.

```python
def empile(pile, valeur):
    return (valeur, pile)

nouvelle_pile = empile(pile_avec_un_element, 4)

```

Au contraire, l'action de dépiler consiste à séparer le sommet du reste de la pile.

```python
def depile(pile):
    return pile[0], pile[1]

sommet, nouvelle_pile = depile(nouvelle_pile, 4)
```

### Approche récursive d'une liste

A l'instar des piles, les listes chainées peuvent être abordées par une définition récursive.

Un liste peut être définie par deux parties :
* la tête de la liste, appelée traditionnellement `car`, qui est la première valeur
* la queue de la liste, appelée traditionnellement `cdr`, qui est elle-même un liste constituée d'une tête `car` et d'une queue `cdr`.


Ainsi rechercher une valeur dans la liste revient à :
* comparer la valeur recherché avec la tête `car`,
* sinon, rechercher cette valeur dans la liste `cdr`.

Ce qui donnerait avec une approche objet :
```python
def rechercher(liste, valeur):
    if liste == None :  # cas trivial
        return False
    elif valeur == liste.car :
        return True
    else :
        return rechercher(liste.cdr, valeur)
```
