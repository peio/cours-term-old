def parcours_en_largeur(graphe, sommet):
    """
    Parcours d'un graphe en largeur
    :param graphe: une liste d'adajence du graphe étudié (un dictionnaire)
    :param sommet: le sommet de départ du graphe étudié
    :return: la liste des sommets du graphe
    >>> parcours_en_largeur({"A":("B","D","E"),"B":("A","C"),"C":("B","D"),
    "D":("A","C","E"),"E":("A","D","F","G"),"F":("E","G"),"G":("E","F","H"),
    "H":("G")},"A")
    ['A', 'B', 'D', 'E', 'C', 'F', 'G', 'H']
    """
    if not(sommet in graphe.keys()):
        return None
    file = [sommet]
    liste_sommets = ...
    ...





    return liste_sommets

# Test
assert parcours_en_largeur({"A":("B","D","E"),"B":("A","C"),"C":("B","D"),
    "D":("A","C","E"),"E":("A","D","F","G"),"F":("E","G"),"G":("E","F","H"),
    "H":("G")},"A") == ['A', 'B', 'D', 'E', 'C', 'F', 'G', 'H']
