def parcours_en_largeur(graphe, sommet):
    """
    Parcours d'un graphe en largeur
    :param graphe: une liste d'adajence du graphe étudié (un dictionnaire)
    :param sommet: le sommet de départ du graphe étudié
    :return: la liste des sommets du graphe

    >>> parcours_en_largeur({"A":("B","D","E"),"B":("A","C"),"C":("B","D"),
    "D":("A","C","E"),"E":("A","D","F","G"),"F":("E","G"),"G":("E","F","H"),
    "H":("G")},"A")
    ['A', 'B', 'D', 'E', 'C', 'F', 'G', 'H']
    """
    if not(sommet in graphe.keys()):
        return None
    file = [sommet]
    liste_sommets = []
    while len(file) != 0:
        S = file[0]
        for voisin in graphe[S]:
            if not(voisin in liste_sommets) and not(voisin in file):
                file.append(voisin)
        liste_sommets.append(file.pop(0))
    return liste_sommets

