#Tests
assert parcours_en_profondeur({"A":("B","D","E"),"B":("A","C"),"C":("B","D"),
"D":("A","C","E"),"E":("A","D","F","G"),"F":("E","G"),"G":("E","F","H"),
"H":("G")},"A") == ['A', 'E', 'G', 'H', 'F', 'D', 'C', 'B']

# Autres tests
graphe = {"A":("B","D","E"),"B":("A","C"),"C":("B","D"),"D":("A","C","E"),
"E":("A","D","F","G"),"F":("E","G"),"G":("E","F","H"),"H":("G")}
assert parcours_en_profondeur(graphe, "B") == ['B', 'C', 'D', 'E', 'G', 'H', 'F', 'A']
assert parcours_en_profondeur(graphe, "C") == ['C', 'D', 'E', 'G', 'H', 'F', 'A', 'B']
assert parcours_en_profondeur(graphe, "D") == ['D', 'E', 'G', 'H', 'F', 'C', 'B', 'A']
assert parcours_en_profondeur(graphe, "E") == ['E', 'G', 'H', 'F', 'D', 'C', 'B', 'A']
assert parcours_en_profondeur(graphe, "F") == ['F', 'G', 'H', 'E', 'D', 'C', 'B', 'A']
assert parcours_en_profondeur(graphe, "G") == ['G', 'H', 'F', 'E', 'D', 'C', 'B', 'A']
assert parcours_en_profondeur(graphe, "H") == ['H', 'G', 'F', 'E', 'D', 'C', 'B', 'A']


