# Tests
f = trinome(1, 1, 1)
assert f(2) == 7
assert f(0) == 1
assert trinome(3, -1, 2)(6) == 104

# Autres tests
for a in range(-5, 6):
    if a != 0:
        for b in range(-5, 6):
            for c in range(-5, 6):
                f = trinome(a, b, c)
                for x in range(-10, 11):
                    attendu = a*x*x + b*x + c
                    assert f(x) == attendu, f"On devrait avoir trinome({a}, {b}, {c})(x) == {attendu} et pas {f(x)}"
