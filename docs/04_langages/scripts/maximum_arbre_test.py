# Tests
assert maximum(((None, 6, None), 15, None)) == 15

# Autres tests
assert maximum(((((None, 3, None), 8, (None, 5, None)), 19, ((None, 7, None), 11, (None, 4, None))))) == 19
assert maximum(((((None, 23, None), 8, (None, 5, None)), 19, ((None, 7, None), 11, (None, 4, None))))) == 23
