def maximum(arbre):
    if est_vide(arbre):
        return 0
    elif taille(arbre) == 1:
        return racine(arbre)
    return(max(racine(arbre), maximum(gauche(arbre)), max(racine(arbre), maximum(droite(arbre)))))
