def ajout_1(ma_liste, i):
  ma_liste.append(i)

def ajout_2(ma_liste, i):
  return ma_liste + [i]

une_liste = [4, 7, 3]
print("Avant appel de la fonction ajout_1 une_liste = ", une_liste)
ajout_1(une_liste, 6)
print("Après appel de la fonction ajout_1 une_liste = ", une_liste)

# Idem avec l'autre fonction

une_liste = [4, 7, 3]
print("Avant appel de la fonction ajout_2 une_liste = ", une_liste)
nouvelle_liste = ajout_2(une_liste, 6)
print("Après appel de la fonction ajout_2 une_liste = ", une_liste)
print("Nouvelle liste créée : ", nouvelle_liste)
