---
authors: Pierre Marquestaut
title: "Debbugage"
---

## Découverte des exceptions

!!! note "Travail à faire"
    Chaque programme ci-dessous présente une erreur engendrant une exception.

    Pour chaque programme :

    - **Identifier** l'erreur générée en exécutant le code ;
    - **Décrire** succintement la nature de l'erreur ;
    - **Proposer** une correction pour le programme puisse s'exécuter


??? question "Programme 1"

    ```python title=""
    #Programme buggé
    def somme_des_inverses(n) :
        '''Calcul la somme 1/1 + 1/2 + ... + 1/n'''
        total = 0
        for i in range(n) :
            total = total + 1/i
        return total

    somme(4)
    ```
    {{IDE('scripts/prog_bug1')}}

??? question "Programme 2"

    ```python title=""
    #Programme buggé
    tableau = [5, 4, 6, 9, 8]
    for i in range(6):
        tableau[i] = tableau[i]*2
    ```

    {{IDE('scripts/prog_bug2')}}

??? question "Programme 3"

    ```python title=""
    #Programme buggé
    def moyenne(tab_valeurs):
        somme = 0
        for val in tab_valeurs :
            somme = somme + val
        moyenne = somme/len(tab_valeurs)
        return "la moyenne est de "+ moyenne

    notes = [15, 12, 10, 16]
    moyenne(notes)
    ```

    {{IDE('scripts/prog_bug3')}}

??? question "Programme 4"

    ```python title=""
    #Programme buggé
    def nombreMax(a, b):
        if a > b :
            return a
        else :
            return b

    nombremax(5, 6)
    ```
    {{IDE('scripts/prog_bug4')}}

??? question "Programme 5"

    ```python title=""
    #Programme buggé
    class Joueur():
        def __init__(self):
            self.vivant = True
            pointdevie = 10
            
        def touche(self):
            pointdevie -= 1
            
    joueur1 = Joueur()
    joueur1.touche()
    ```
    {{IDE('scripts/prog_bug5')}}

??? question "Programme 6"

    ```python title=""
    #Programme buggé
    class Joueur():
        def __init__(self):
            self.pointdevie = 10
            
        def touché(self):
            if self.pointdevie == 0:
                self.vivant = False
            
    joueur1 = Joueur()
    joueur1.touche()
    ```
    {{IDE('scripts/prog_bug6')}}

??? question "Programme 7"

    ```python title=""
    #Programme buggé
    paiement = {}
    paiement["Paul"] = 22
    paiement["Claudine"] = 15
    paiement["Patxi"] = 18
    total = paiement["Paulo"]+paiement["Claudine"]+paiement["Patxi"]
    ```
    {{IDE('scripts/prog_bug7')}}

## Mise en pratique

Le programme suivant présente de nombreux bugs.

```python title=""
class Balle:
    '''Classe pour définir une balle graphique'''
    couleur = {"rouge":(255,0,0), "vert":(0,255,0), "bleu":(0,0,255)}
    def __init__(self, couleur):
        self.couleur = couleur
        self.x = 100
        self.y = 100
        
    def deplacer(self):
        '''Mise à jour de la position de la balle par incrémentation des coordonnées'''
        x += 1
        y += 1
    
    def __str__(self):
        return "Position de la balle :"+self.x+", "+self.y
    
#Création de trois balles de couleurs différentes
balles = [Ballon("jaune"), Ballon("rouge"), Ballon("vert")]

for i in range(4):
    balles[i].deplace()
print(balles[0])   
```

Corriger le programme pour qu'il puisse s'exécuter sans erreur.

{{IDE('scripts/prog_bug8')}}




