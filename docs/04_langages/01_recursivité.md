---
author: Franck Chambon, Mireille Coilhac, Valérie Mousseaux, Jean-Louis Thirot
title: Les fonctions récursives
---

# Les fonctions récursives




## I. La récursivité en générale

### I.1 Dans la culture

La **mise en abyme** est un procédé consistant à représenter une œuvre dans une œuvre similaire, par exemple dans les phénomènes de « film dans un film », ou encore en incrustant dans une image cette image elle-même (en réduction). 


!!! danger "Mise en abyme multiple"

    ![vache](images/vache.gif){ width=30% }
    > Création de l'artiste visuel Polonais Feliks Tomasz Konczakowski

### I.2 Dans la nature

Le principe de mise en abime se retrouve dans le concept d'**autosimilarité**, qui se réfère un objet qui ressemble à une de ses partie.

Remarquons qu'un chou romanesco est constitué d'un ensemble de « florettes » pyramidales disposées en couronnes spiralées. 

Un de ses florettes prise séparemment ressemble au choux lui-même. C'est le principe de l'autosimilarité. 

![Romanesco](images/Romanesco.jpg){ width=20% }

Crédits : [Ivar Leidus - CC BY-SA 4.0 -  via Wikimedia Commons](https://commons.wikimedia.org/wiki/File:Romanesco_broccoli_(Brassica_oleracea).jpg)

On retrouve ce concept dans le principe des figures géométriques fractales ou du principe mathématique de la récursivité.




### I.3 Dans la culture informatique



Dans la culture informatique, la récursivité est (trop) souvent abordée par le biais de l'auto-référence, le puits sans fin de la boucle infinie.

On trouve d'ailleurs fréquemment cette définition de la récursivité :

!!! abstract "Définition"
    
    Une **fonction récursive :** fonction qui fait appel à la récursivité. Voir fonction récursive.
    

Les *acronymes récursifs* sont aussi très fréquents... et véhiculent avec eux le même piège : une fonction récursive ne serait jamais vraiment définie (c'est faux, nous le verrons)

Par exemple :

* GNU (dans GNU/Linux) signifie GNU is Not Unix. On ne sait jamais vraiment ce que signifie GNU...
* PHP (le langage serveur) sigifie PHP: Hypertext Preprocessor
* VISA (les cartes bancaires) signifie VISA International Service Association.


## V. En informatique

En informatique, on trouve :

- des fonctions récursives, l'objet de ce chapitre ;
- des structures de données récursives, comme une pile ou certains arbres, l'objet de chapitres suivants ;
- des fractales ; nous apprendrons à en dessiner certaines.

!!! abstract "le flocon de Koch"

    Le flocon de Koch est l'une des premières courbes fractales à avoir été décrites, bien avant l'invention du terme « fractal(e) » par Benoît Mandelbrot.

    ![Koch](images/Koch_anime.gif){ width=30% }

    Elle a été inventée en 1904 par le mathématicien suédois Helge von Koch.


!!! abstract "La fougère de Barnsley"

    La fougère de Barnsley est une fractale nommée d'après le mathématicien Michael Barnsley qui l'a décrite pour la première fois dans son livre Fractals Everywhere.

    ![Fougère](images/Barnsleys_fern.png){ width=20% }

!!! abstract "L'arbre de Pythagore"

    L'arbre de Pythagore est une fractale plane construite à l'aide de carrés. Elle porte le nom de Pythagore car chaque triplet de carrés en contact enclot un triangle rectangle, une configuration traditionnellement utilisée pour illustrer le théorème de Pythagore. 

    ![Arbre Pythagore](images/Pythagoras_tree_1_1_13_Summer.svg){ width=30% }





## II Les fonctions récursives

!!! abstract "Définition"

    Une fonction est dite récursive si elle contient un appel à elle-même dans sa propre définition.


???+ question "Question 1"

    Que fait ce script ?

    ```python
    def f_1():
        print("Bonjour")
        f_1()

    f_1()
    ```

    ??? success "Solution"

        Ce script affiche "Bonjour" de nombreuses fois.

        En théorie jusqu'à l'infini, mais Python arrête l'exécution au bout d'un moment et affiche un message d'erreur.
        

???+ question "Question 2"

    Que fait ce script ?


    ```python
    def f_2():
        f_2()
        print("Bonjour")

    f_2()
    ```

    ??? success "Solution"

        Ce script n'affiche jamais "Bonjour", mais...

        Il y a de nombreux appels récursifs, jusqu'au message d'erreur de Python


???+ question "Question 3"

    Que fait ce script ?


    ```python
    def f_3(n):
        print(n)
        if n > 0:
            f_3(n - 1)

    f_3(4)
    ```

    ??? success "Solution"

        Tester :

        {{IDE('scripts/f_3')}}

    
!!! info "Condition d'arrêt"

    Une fonction récursive étant une fonction qui s'appelle elle-même, on peut créer une fonction qui "tourne" théoriquement indéfiniment.  

    Python stoppe de lui même au bout d'un trop grand nombre d'appels de la fonction.

## III. Le principe



Bien lire et comprendre la bande dessinée suivante, dessinée par Gilles LASSUS :

![BD Gilles Lassus](images/bd_recurs.png){ width=70% }


Il est important de comprendre que chaque appel récursif met « en pause » l'exécution en cours, en attente d'obtenir le résultat qui est déterminé par l'appel suivant. Concrètement :

* Les appels sont tour à tour mis « en pause » jusqu'au dernier appel qui fourni un résultat. On appelle cela le dépliage (ou la descente).

* Ce résultat est ensuite transmis à l'appel précédent qui l'utilise pour calculer son propre résultat et le transmettre à l'appel précédent, et ainsi de suite jusqu'au premier appel qui peut alors calculer le résultat final. On appelle cela l'évaluation (ou la remontée).

!!! example "Exemple"

	Nous allons étudier la fonction récursive (naïve) `deux_puissance` qui prend en paramètre un entier positif `n` et qui renvoie la valeur de $2^n$

	Ecriture de la fonction récursive

	* Le cas de base correspond à n = 0 et dans ce cas $2^n=2^0=1$
	* Sinon, on peut calculer $2^n$ en calculant $2 \times 2^{n-1}$

	👉 on sait donc comment passer du calcul de $2^n$ à celui de $2^{n-1}$ pour notre appel récursif et on connaît le cas de base qui sera notre condition d'arrêt de la récursion

	![2 puissance n](images/2_puiss_n.png){ width=50% }

???+ question "Tester"

    Tester

    {{IDE('scripts/2_puiss_n')}}


!!! info "Comment concevoir une fonction récursive"

    1. Trouver le cas général : trouver l'élément de récursivité qui permet de décomposer le problème en cas plus simple.
    2. Trouver le cas de base : trouver la condition d'arrêt, et la solution du problème dans ce cas particulier.  

        * La condition d'arrêt sera bien atteinte après un nombre **fini** d'appels.  
        * Chaque appel récursif tend à s'approcher du cas de base.

    3. Réunir ces deux cas dans le programme :   

        * Le programme ne contient aucune boucle (`for` ou `while`) dans la partie qui résoud le problème.
        * Le programme contient un général une structure `if` / `else` introduisant le cas de base.


## IV. Arbre des appels

!!! abstract "Arbre des appels"

    L'**arbre des appels** récursifs est une autre abstraction algorithmique de l'ensemble des appels d'un traitement récursif.


!!! example "Représentation 1"

	![nom image](images/appel_1.png){ width=80% }

!!! example "Représentation 2"

	![nom image](images/rep_2.png){ width=80% }

!!! example "Représentation 3"

	![nom image](images/rep_3.png){ width=80% }




!!! info "Visualisation avec Python tutor"

	<iframe width="800" height="500" frameborder="0" src="https://pythontutor.com/iframe-embed.html#code=def%20deux_puissance%28n%29%3A%0A%20%20%20%20if%20n%20%3D%3D%200%3A%0A%20%20%20%20%20%20%20%20return%201%0A%20%20%20%20else%20%3A%0A%20%20%20%20%20%20%20%20return%202*deux_puissance%28n%20-%201%29%0A%0Aprint%28deux_puissance%283%29%29&codeDivHeight=400&codeDivWidth=350&cumulative=false&curInstr=0&heapPrimitives=nevernest&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false"> </iframe>

	
!!! question "Arbre des appels"

	<div class="centre" markdown="span">
		<iframe 
			src="{{ page.canonical_url }}../activites/appels.html"
			width="100%" height="600"
			id="classe"
			title="Classe" 
			frameborder="0" 
			allow="autoplay; fullscreen; picture-in-picture">
		</iframe>
	</div>

## V. Itératifs et récursifs

!!! note "Remarques"
    On appelle **fonction itérative** une fonction qui s'écrit avec une boucle.

    Toute fonction itérative peut être écrite par une fonction récursive.

!!! warning "Fonction récursive et boucle"
    Au contraire, une fonction récursive ne contiendra pas de boucle.

    
??? question "Exemple 1" 
    <iframe src="https://www.codepuzzle.io/IPRZET" width="100%" height="450" frameborder="0"></iframe>
    <iframe src="https://www.codepuzzle.io/IPM6LN" width="100%" height="450" frameborder="0"></iframe>

??? question "Exemple 2"   
    <iframe src="https://www.codepuzzle.io/IP6DMN" width="100%" height="600" frameborder="0"></iframe>
    <iframe src="https://www.codepuzzle.io/IPK7UH" width="100%" height="600" frameborder="0"></iframe>



## IV. Exemple de fractale

Nous désirons créer une fonction qui trace ceci en utisant la bibliothèque `turtle`.  
Il s'agit de triangles de Sierpiński.

???+ note dépliée "Exécuter une cellule d'un notebook jupyter"

    Cliquer sur la touche  ![run](images/run.png){ width=2% } dans la barre d'outil à gauche de la cellule


![triangles](images/triangles.png){ width=25% }



<div class="centre">
<iframe 
src=
https://notebook.basthon.fr/?ipynb=eJzFVMtu2zAQ_JUFc5EKpfUjvQjwP-TQnqrAYMlVTIRaEnw0NQx_T5Hv8I91GbuxHCBAmjaJTtwdamYwXHIjFFobRfttIwZMUsskRbvZNvf9ZVp7FK0YZLjR7pZEI6LLQZXelyB3vxxFyATRDN4ipGAkXVv8KLbNCZ1IIceEWrS8wEfkymkcE_fBDZBySMzIvC4k-NBRR70LYMAQBBbBal63HQF_3L-VQVezyaTedyz2qZrOSjlIQ9Y5X9UsgT9R5WQcLZXLlERL2dpGuJx8TiWDq0e-nxHD12SsifsckL2QKgIQdncqh2h-IHjeytFIhQG8zdEgAw9ZxVfJSmMPg0umr5RL2ADVcEjL9ECwWMD0T-MQ4Wm0Y3CccWGrT6FR2KVEGxFa-Avuo89PM3YK5zCtX6Le0dnKaNynUdUAZ8AHoaRaIVgJicPJyJuiR9TVpGZ832Nnnn_sqPDDAniQXmlYHu6MNn2_uwtI6X9Nwvi0p_88-C-Wnr2f9Pz9pC_eVvrpCR6b-vy2pp4ncNWcCNxgIH4xPKpSkRwKk1-nlaM5E2oTvZXr5QG4vAegIJavTJbXx-1iy_7oOz8Vg2Tli2OxHAy5INrZ9jff2FnB
width="900" height="900" 
frameborder="0" 
allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
</iframe>
</div>


