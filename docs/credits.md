---
author: Pierre Marquestaut
title: Crédits
---

Ce site a été réalisé par P. Marquestaut. Les sources des fichiers et bases de données sont fournies dans chaque page.

Je remercie les différents professeurs d'informatique pour leur travail dont ses présents cours s'inspirent ainsi que pour leurs conseils et patientes relectures :  [G. Lassus](https://glassus.github.io/terminale_nsi/), [M. Coilhac](https://mcoilhac.forge.aeif.fr/term/), N. Riveret, R. Janvier, C. Poulemaire

Le site est hébergé par la forge de [l'*Association des Enseignants d'Informatique de France*](https://aeif.fr/index.php/accueil/). Le dépôt de ce projet est [ici](https://forge.aeif.fr/Peio/cours-Term).

Les illustrations sans crédit sont dans le domaine public.

Le logo et l'icône ont été créés par [Freepik - Flaticon](https://www.flaticon.com/free-icons/sql).


Le site est construit avec [`mkdocs`](https://www.mkdocs.org/) et en particulier [`mkdocs-material`](https://squidfunk.github.io/mkdocs-material/).

Les activités H5P ont été développées avec l'application [Logiquiz by La Digitale](https://ladigitale.dev/logiquiz/). Ces activités sont essentiellement hébergées directement mais certaines peuvent l'être sur [Digiquizz](https://ladigitale.dev/digiquiz/)

Les puzzles ont été créés sur le site https://www.codepuzzle.io de Laurent Abbal.

La partie `SQL` est mise en place grâce à l'excellente extension [`mkdocs-sqlite-console`](https://github.com/Epithumia/mkdocs-sqlite-console). Un très grand merci à l'auteur !