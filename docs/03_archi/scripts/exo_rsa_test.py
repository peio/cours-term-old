# Tests
assert RSA((3, 33), 4) == 31
assert RSA((7, 33), 31) == 4

# Autres tests
assert RSA((7, 187), 42) == 15
assert RSA((23, 187), 15) == 42
assert RSA((7, 187), 15) == 93
assert RSA((23, 187), 93) == 15



