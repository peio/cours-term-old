---
authors: Angélique Beucher, Jean-Louis Thirot, Valérie Mousseaux, Gilles Lassus et Mireille Coilhac, Olivier Lecluse
title: "Protocoles de routage"
---

# Protocoles de routage


## I. Quelques rappels


!!! info "Modèle client-serveur" 

    Les clients échangent des informations avec les serveurs. La notion de client recouvre aussi bien une machine, qu'une application. 

    De même le serveur désigne aussi bien l'ordinateur qui héberge un service que le logiciel qui fournit ce service.


!!! info "Protocole"

    Les protocoles sont l'ensemble des règles utilisées par les clients et les serveurs pour communiquer.


!!! info "Paquets"

    Les informations échangées sont découpées en paquets de petites tailles. Ces paquets sont envoyés séparément et transitent sur le réseaux indépendamment les uns des    autres.
    Ils peuvent parvenir à destination dans le désordre, le protocole doit donc prévoir des fonctions pour reconstituer l'information. 


## II. Routeurs et topologie d'un réseau

En plus des clients et des serveurs, le réseau est constitué de switchs (commutateurs) et de routeurs.  
Ces machines reçoivent et transmettent l'information vers la destination.

Les clients et les serveurs communiquent avec les routeurs (Ethernet, wifi...)


!!! info "Adresse IP"

    &#128073; Chaque machine sur le réseau est identifiée par une adresse IP. La norme actuelle est IPv4, la migration est en cours
    vers IPv6.

    Dans la norme IPv4 l'adresse est codée sur 32 bits (4 octets), soit 4 entiers de 0 à 255 (car 255 en base 10 s'écrit 1111 1111 en base 2)

    Exemple d'adresse IP : 168.1.2.10


!!! info "Masque de sous réseau : notations"
    
    L'adresse IP est en réalité composée de **2 parties** : l'adresse **du réseau**, et l'adresse **de la machine sur le réseau**. 


    &#128073; Le masque permet de connaitre la partie réservée à l'adresse du réseau.

    Il existe deux façons de noter le masque. 

    ???+ note "Première notation possible avec l'adresse du masque"

        IP = 168.1.2.5   
        mask = 255.255.255.0 

        Dans cet exemple très simple, les valeurs 255 indiquent la partie réseau, les 0 indiquent la partie identifiant la machine sur le réseau. 

        ![](images/res_mach.png){ width=20% }

        Dans l'exemple ci-dessus 168.1.2.0 est l'adresse du réseau, seul le dernier nombre identifie une machine.

        Dans ce cas-là, toute adresse 168.1.2.x identifiera une machine sur le réseau. x peut prendre 256 valeurs, ce sous réseau peut donc 
        connecter 254 machines car il y a deux **adresses réservées** :  
        
        * 👉 adresses réservées 168.1.2.0 celle du réseau, obtenue avec uniquement des 0 sur la plage d'adressage machines
        * 👉 168.1.2.255 celle du broadcast (adresse réservée pour une diffusion sur toutes les machines du réseau) obtenue avec uniquement des 1 sur la plage d'adressage  machines.  

    ???+ note "Deuxième notation possible : notation CIDR"

        &#128073; On peut aussi noter le masque de la façon suivante : 168.1.2.0/24

        **/24** indique que 24 bits sont utilisés pour l'adresse réseau. Les 8 bits restant codent donc l'adresse machine.

        &#128512; On peut avec cette notation plus simple, utiliser une très faible quantité d'information pour l'adresse machine. Cela est
        préconisé pour un routeur, qui ne gère généralement pas un très grand nombre de machines.

        10.1.2.5/30 indiquera par exemple que l'adresse réseau est codée sur 30 bits, seulement 2 bits (4 valeurs possibles) sont utilisables pour des machines. C'est très    peu !



??? example " Un exemple détaillé"

    Considérons la machine dont la configuration réseau est : IP : 172.128.10.5 ; Masque : 255.255.192.0 .  
    On écrit de façon plus courante : 172.128.10.5/18  
    On obtient l'adresse du sous réseau avec l'opérateur AND.  

    Écrivons en binaire l'adresse IP et le masque :  
    $172.128.10.5$ s'écrit en binaire :  $10101100.10000000.00001010.00000101$  
    $255.255.192.0$ s'écrit en binaire : $11111111.11111111.11000000.00000000$  

    Posons l'opération du ET logique entre l'adresse IP de la machine et le masque :  

    ![calcul de masque](images/ex_calc_masque.jpg){ width=50% .center}

    On met en décimal le résultat : $172.128.0.0$  est l'adresse du réseau.  

    La seconde partie nous permet de savoir combien de machines peut contenir ce réseau.  
    On peut aller de $000000.00000000$ à $111111.11111111$.  
    En décimal : de 0 à 16383. C'est à dire 16384 adresses possibles...  

    &#127797; Enfin pas tout à fait :

    * Il faut retirer l'adresse du réseau lui-même : $172.128.0.0$ 
    * Il faut également retirer (la dernière ) l'adresse de broadcast :  $10101100.10000000.00111111.11111111$ c'est à dire l'adresse $172.128.63.255$


    Donc en tout : on peut connecter 16382 machines dans ce réseau.


???+ question "Exercice"

    On donne l'adresse IP d'un matériel suivante : Adresse IP : 192.168.1.100/16  
    
    Déterminer : 

    1. le masque de sous-réseau
    2. l'adresse du réseau
    3. le nombre de machines que l'on peut connecter à ce réseau
    4. l'adresse broadcast
    5. l'adresse IP de la première machine
    6. l'adresse IP de la dernière machine

    ??? success "Solution"

        1. Le masque de sous-réseau est : 
        255.255.0.0  
            
        2. L'adresse du réseau est donc :  
        192.168.0.0   
              
        3. Le nombre de machines que l'on peut connecter à ce réseau :  
        On peut connecter $2^{16} -2  = 65534$ machines
            
        4. L'adresse broadcast est donc  
        192.168.255.255 
              
        5. L'adresse IP de la dernière machine est : 192.168.255.254  



!!! danger "Deux routeurs liés par un câble"

    &#127797; Même si entre deux routeurs, il n’y a rien d’autre qu’un câble, ce câble constitue un réseau.  
    En pratique ce réseau pourrait avoir pour masque 255.255.255.252 (en CIDR /30) .  
    Sur un tel réseau, il n’y a donc que quatre adresses possibles. Etant donné que deux sont prises pour le réseau et le broadcast, il n’en reste que deux pour connecter des matériels. Ce seront donc les deux adresses IP des routeurs, qui se termineront donc forcément par .1 et .2 (l’adresse se terminant par .0 étant celle du réseau, et celle se terminant par .3 celle du broadcast)  

## III. Tables de routages

![](images/reseau_total2.png){: .center}

Quand une machine (par exemple ici le client) souhaite communiquer avec une autre machine (par exemple le serveur) les paquets doivent transiter par plusieurs routeurs.


!!! abstract "Tables de routage"

    Les tables de routage sont des informations stockées dans le routeur permettant d'aiguiller intelligemment les données qui lui sont transmises.

Dans le réseau ci-dessus, si l'ordinateur d'adresse ```192.168.0.5``` veut interroger le serveur ```10.7.3.8``` :

- l'adresse ```10.7.3.8``` n'étant pas dans le sous-réseau F (d'adresse ```192.168.0.0 / 24```), la requête est confiée au routeur R1 via son adresse passerelle dans le réseau F (ici ```192.168.0.254```).
- le routeur R1 observe si l'IP recherchée appartient à un autre des sous-réseaux auquel il est connecté. Ici, l'IP recherchée ```10.7.3.8``` n'appartient ni au sous-réseau A, ni au sous-réseau E. 
- le routeur R1 va donc regarder dans sa table de routage l'adresse passerelle d'un autre routeur vers qui elle doit rediriger les données. Si le sous-réseau C fait partie de sa table de routage, le routeur R1 saura alors que le meilleur chemin est (par exemple) de confier les données au routeur R3.
- si le sous-réseau C ne fait pas partie de la table de routage, le routeur R1 va alors le rediriger vers une route «par défaut» (que l'on peut assimiler au panneau «toutes directions» sur les panneaux de signalisation).

!!! abstract "interface et passerelle"
    Les tables de routage des routeurs font très souvent apparaître deux colonnes, _interface_ et _passerelle_ (ou _gateway_), dont il ne faut pas confondre l'utilité :

    - **interface** : c'est l'adresse IP de la carte réseau du routeur par où va **sortir** le paquet à envoyer. Il y a donc **toujours** une adresse d'interface à renseigner (car un paquet sort bien de quelque part !). Parfois cette interface sera juste nommée _eth0_, _wlo1_, _interface1_ ou _interface2_.  


    - **passerelle** : c'est l'adresse IP de la carte réseau du routeur **à qui on va confier le paquet**, si on n'est pas capable de le délivrer directement (donc si l'adresse IP de destination n'est pas dans notre propre sous-réseau). Cette adresse de passerelle n'est donc pas _systématiquement_ mentionnée. Quand elle l'est, elle donne le renseignement sur le prochain routeur à qui le paquet est confié.

!!! note "La route par défaut : 0.0.0.0/0"

    Dans le contexte de routage, 0.0.0.0 (ou 0.0.0.0/0 en notation CIDR) signifie généralement la **route par défaut**.

    La destination 0.0.0.0/0 représente donc souvent "le reste d'internet", plutôt qu'une adresse du réseau local.

!!! example "Exemple: table de routage du routeur R1"

    ![](images/reseau_total2.png){: .center width=70%}


    **Table de routage du routeur R1 :**

    | Destination | Interface | Passerelle |
    |:-:|-|-|
    | F | 192.168.0.254 | | 
    | A | 10.0.5.152 | |
    | E | 172.17.1.254 | |  
    | B | 172.17.1.254 |172.17.1.123| 
    | C | 10.0.5.152 |10.0.5.135| 

    Les trois réseaux F, A et E sont directement accessibles au routeur R1, puisqu'il en fait partie : il n'a donc pas besoin d'adresse passerelle pour communiquer avec ces réseaux.

    Par contre, la communication avec le réseau B nécessite de confier le paquet au routeur R2 (c'est le choix de cette table de routage). Il faut donc mentionner l'adresse IP de ce routeur R2 (172.17.1.123), qu'on appelle adresse de passerelle.

    De la même manière, la communication avec le réseau C nécessite de confier le paquet au routeur R3 (c'est le choix de cette table de routage). Il faut donc mentionner l'adresse IP de ce routeur R3 (10.0.5.135).

!!! example "Exemple"

    Table de routage d'un ordinateur sous Windows 7 obtenue avec la commande <font face= "Courier New"><b>route print</b></font></p>

    ![Capture d'&eacute;cran table de routage IP v4 Windows](images/ .png){ width=90% .center}



    Cette table peut se d&eacute;composer de la mani&egrave;re suivante :</p>

	* **Destination :** Plage d'adresse de destination d&eacute;termin&eacute;e par le couple Destination r&eacute;seau / Masque r&eacute;seau
	* **Adresse passerelle :** Adresse du routeur qui permet d'atteindre le r&eacute;seau de destination.</li>
	* **Adresse interface :** Carte r&eacute;seau &agrave; utiliser pour contacter le routeur mentionn&eacute; dans "Adresse passerelle".
	* **Métrique :** Indique le co&ucirc;t relatif de l'itin&eacute;raire pour atteindre la destination</li>
    
    Si on exclu les informations de broadcast, loopback et multicast, la table pr&eacute;c&eacute;dente peut se r&eacute;sumer &agrave; :


    ![Capture d'&eacute;cran table de routage IP v4 Windows simplifiée](images/Routage_03.png){ width=90% .center}

    L'ordre de traitement de la table de routage va des masques les plus longs aux plus petits. C'est à dire que le routeur va d'abord comparer les sous-réseaux avec le masque 255.255.255.255 pour finir par comparer les sous-réseaux avec le masque 0.0.0.0.
    
    <p>La table de routage simplifi&eacute;e ci-dessus peut se traduire ainsi (dans l'ordre de traitement) :</p>
    <ol>
    <li>Route vers l'ordinateur lui-m&ecirc;me, "Destination r&eacute;seau" et "Adresse interface" ont la m&ecirc;me valeur. On remarquera le masque enti&egrave;rement &agrave; 255 (/32 en CIDR) qui permet de d&eacute;signer un r&eacute;seau (une plage) limit&eacute;e &agrave; une seule adresse.</li>
    <li>Route vers le r&eacute;seau local sur lequel est connect&eacute; l'ordinateur. "On-link" indique que l'ordinateur est directement connect&eacute; au r&eacute;seau concern&eacute;, il n'y a donc pas besoin de routeur pou l'atteindre.</li>
    <li>Route par d&eacute;faut 0.0.0.0/0.0.0.0 : c'est la route utilis&eacute;e si aucune autre route possible n'a &eacute;t&eacute; trouv&eacute;e dans la table de routage.  </li>
    </ol></p>
    <p>Source : 
    <a target="wims_external" href="https://www.electro-info.ovh/index.php?id=137">https://www.electro-info.ovh/index.php?id=137</a></p>

???+ question "&#128187; A vous de jouer chez vous :"

    <p>Sur votre ordinateur personnel, chez vous, dans la barre de recherche du bas, saisissez <font face= "Courier New"><b>cmd</b></font>, puis une fois l'invite de commande  apparue, saisissez <font face= "Courier New"><b> route print </b></font>
    <br> Essayez de comprendre ce qui s'affiche.</p>

    <p><b>Remarque</b>
    <br>sous GNU/Linux, la commande <font face= "Courier New"><b>route -n </b></font> renvoie le même style de tableaux.</p>


<div class="centre" markdown="span">
    <iframe 
        src="{{ page.canonical_url }}../activites/tables_routage.html"
        width="950" height="700"
        id="quizz"
        title="Vocabulaire" 
        frameborder="0" 
        allow="autoplay; fullscreen; picture-in-picture">
    </iframe>
</div>


#### Comment sont construites les tables de routage ?
- Soit à la main par l'administrateur réseau, quand le réseau est petit : on parle alors de table **statique**. Cependant, en cas de changement du réseau (ajout d'un nouveau routeur, panne d'un routeur, etc...) il faudrait
    alors maintenir à la main la table de routage. Cela serait sans doute faisable dans un tout petit réseau, mais le nombre de routeurs d'un réseau est généralement beaucoup trop grand pour qu'on puisse envisager cela, même avec un nombre important
    d'humains passant leur temps à communiquer entre eux les changements pour mettre à jour les tables.
 
- Soit de manière **dynamique** : les réseaux s'envoient eux-mêmes des informations permettant de mettre à jour leurs tables de routages respectives. Des algorithmes de détermination de meilleur chemin sont alors utilisés : nous allons en découvrir deux, le protocole RIP et le protocole OSPF.

## 2. Le protocole RIP



!!! abstract "Les règles du protocole RIP :heart: :heart: :heart:"
    Le Routing Information Protocol (RIP) est basé sur l'échange (toutes les 30 secondes) des tables de routage de chaque routeur.  
    Au début, chaque routeur ne connaît que les réseaux auquel il est directement connecté, associé à la distance 1.  
    Ensuite, chaque routeur va recevoir périodiquement (toutes les 30 secondes) la table des réseaux auquel il est connecté, et mettre à jour sa propre table suivant les règles ci-dessous :

    - s'il découvre une route vers un nouveau réseau inconnu, il l'ajoute à sa table en augmentant de 1 la distance annoncée par le routeur qui lui a transmis sa table.

    - s'il découvre une route vers un réseau connu mais plus courte (en rajoutant 1) que celle qu'il possède dans sa table,  il actualise sa table.

    - s'il découvre une route vers un réseau connu mais plus longue que celle qu'il possède dans sa table, il ignore cette route.

    - s'il reçoit une route vers un réseau connu en provenance d'un routeur déjà existant dans sa table, s'il met à jour sa table car la topologie du réseau a été modifiée.

    - si le réseau n'évolue pas (panne ou ajout de nouveau matériel), les tables de routage _convergent_ vers une valeur stable. Elles n'évoluent plus.

    - si un routeur ne reçoit pas pendant 3 minutes d'information de la part d'un routeur qui lui avait auparavant communiqué sa table de routage, ce routeur est considéré comme en panne, et toutes les routes passant par lui sont affectées de la distance infinie : 16.


!!! question "Qu'est-ce que le protocole RIP optimise ?"
    Le protocole RIP cherche à minimiser **le nombre de sauts**, donc le nombre de routeurs traversés. 

    En extrapolant un peu, on peut dire que RIP minimise **la distance parcourue**.


**Remarques et inconvénients:** 

- Le protocole RIP n'admet qu'une distance maximale égale à 15 (ceci explique que 16 soit considéré comme la distance infinie), ce qui le limite aux réseaux de petite taille.

- Chaque routeur n'a jamais connaissance de la topologie du réseau tout entier : il ne le connaît que par ce que les autres routeurs lui ont raconté. On dit que ce protocole de routage est du _routing by rumor_.

- La _métrique_ utilisée (le nombre de sauts) ne tient pas compte de la qualité de la liaison, contrairement au protocole OSPF.    



## 3. Le protocole OSPF

OSPF : *Open Shortest Path First*


Un inconvénient majeur du protocole précédent est la non-prise en compte de la bande passante reliant les routeurs.



!!! abstract "principe fondamental du protocole OSPF :heart:"
    Le chemin le plus rapide n'est pas forcément le plus court.

![image](images/maps.png){: .center width=40%}

<div>
<center>
<i>
En gris, le chemin RIP. En bleu, l'OSPF.
</i>
</center>
</div>






Dans le protocole OSPF, les tables de routage vont prendre en considération la vitesse de communication entre les routeurs.

Dans une première phase d'initialisation, chaque routeur va acquérir (par succession de messages envoyés et reçus) la connaissance **totale** du réseau (différence fondamentale avec RIP) et de la qualité technique de la liaison entre chaque routeur.

#### 3.1 Les différents types de liaison et leur coût
On peut, approximativement, classer les types de liaison suivant ce tableau de débits **théoriques** :


| Technologie | BP descendante | BP montante |
|-|-|-|
| Modem | 56 kbit/s | 48 kbit/s |
| Bluetooth | 3 Mbit/s | 3 Mbit/s |
| Ethernet | 10 Mbit/s | 10 Mbit/s |
| Wi-Fi |  10 Mbit/s ~ 10 Gbits/s | 10 Mbit/s ~ 10 Gbits/s |
| ADSL | 13 Mbit/s | 1 Mbit/s |
| 4G | 100 Mbit/s | 50 Mbit/s |
| Satellite | 50 Mbit/s | 1 Mbit/s |
| Fast Ethernet | 100 Mbit/s | 100 Mbit/s |
| FFTH (fibre) | 10 Gbit/s | 10 Gbit/s |
| 5G | 20 Gbit/s | 10 Gbit/s |



L'idée du protocole OSPF est de pondérer chaque trajet entre routeurs (comptant simplement pour «1» dans le protocole RIP) par une valeur de **coût** inversement proportionnelle au débit de transfert.

Par exemple, si le débit $d$ est exprimé en bits/s, on peut calculer le coût de chaque liaison par la formule :

$$ \text{coût} = \frac{10^8}{d} $$


Cette formule de calcul peut être différente suivant les exercices, et sera systématiquement redonnée. Néanmoins la valeur $d$ sera toujours au dénominateur, pour assurer la proportionnalité inverse du débit.


Avec cette convention, un route entre deux routeurs reliés en Fast Ethernet (100 Mbits/s) aura a un poids de 1, une liaison satellite de 20 Mbits/s aura un poids de 5, etc.

??? question "QCM puissances - 1"

    $10^0=$

    === "Cocher la ou les affirmations correctes"
        
        - [ ] 0
        - [ ] 1
        - [ ] 10
        - [ ] Autre réponse

    === "Solution"
        
        - :x: 
        - :white_check_mark: A savoir : $a^0=1$
        - :x: 
        - :x: 

??? question "QCM puissances - 2"

    $\dfrac{10^8}{10^6}=$

    === "Cocher la ou les affirmations correctes"
        
        - [ ] 2
        - [ ] 100
        - [ ] 0.01
        - [ ] Autre réponse

    === "Solution"
        
        - :x: 
        - :white_check_mark: $\dfrac{10^8}{10^6}=10^{8-6}=10^{2}=100$
        - :x: 
        - :x: 

??? question "QCM puissances - 3"

    $\dfrac{10^6}{10^8}=$

    === "Cocher la ou les affirmations correctes"
        
        - [ ] 2
        - [ ] 100
        - [ ] 0.01
        - [ ] Autre réponse

    === "Solution"
        
        - :x: 
        - :x:
        - :white_check_mark: $\dfrac{10^6}{10^8}=10^{6-8}=10^{-2}=\dfrac{1}{10^2}=\dfrac{1}{100}=0.01$
        - :x: 

??? question "QCM puissances - 4"

    $\dfrac{10^8}{50 \times 10^6}=$

    === "Cocher la ou les affirmations correctes"
        
        - [ ] 2
        - [ ] 0.2
        - [ ] 20
        - [ ] Autre réponse

    === "Solution"
        
        - :white_check_mark: $\dfrac{10^8}{50 \times 10^6}=\dfrac{10^2 \times 10^{6}}{50 \times 10^6}=\dfrac{100 \times 10^{6}}{50 \times 10^6}= 2$
        - :x:
        - :x:
        - :x: 

??? question "Unités"

	Donner en bps ($\text{b.s}^{-1}$)

	a) 2 kbps

	b) 50 kbps

	c) 3 Mbps

	d) 50 Mbps

	e) 100 Mbps

	f) 1 Gbps

	g) 10 Gbps

	??? success "Solution"

		a) 2 kbps = $2 \times 10^3$ bps = 2000 bps = 2000 b/s = 2000 $\text{b.s}^{-1}$

		b) 50 kbps = $50 \times 10^3$ bps = 50000 bps = $5 \times 10^4$ bps  = $5 \times 10^4$  $\text{b.s}^{-1}$

		c) 3 Mbps =  $3 \times 10^6$ bps = $3 \times 10^6$ b/s = $3 \times 10^6$  $\text{b.s}^{-1}$

		d) 50 Mbps = $50 \times 10^6$ bps = $50 \times 10^6$ b/s =  $5 \times 10^7$ b/s = $5 \times 10^7$   $\text{b.s}^{-1}$

		e) 100 Mbps = $100 \times 10^6$ bps = $10^2 \times 10^6$ bps = $10^8$ bps = $10^8$ b/s = $10^8$   $\text{b.s}^{-1}$

		f) 1 Gbps = $10^9$ bps = $10^9$ b/s = $10^9$   $\text{b.s}^{-1}$

		g) 10 Gbps = $10 \times 10^{9}$ bps = $10^{10}$ b/s = $10^{10}$   $\text{b.s}^{-1}$






??? question "Exercice 1"

    a) Calculer la métrique OSPF d'une liaison Fibre avec une valeur de référence de $10^8$ bps 

	b) Même question avec une valeur de référence de $10^{10}$ bps

    ??? success "Solution"

		a) $\dfrac{10^8}{10^{10}}=10^{8-10}=10^{-2}=0.01$

		b) $\dfrac{10^{10}}{10^{10}}=1$

??? question "Exercice 2"

    a) Calculer la métrique OSPF d'une liaison FastEthernet avec une valeur de référence de $10^8$ bps 

	b) Même question avec une valeur de référence de $10^{10}$ bps

    ??? success "Solution"

		100 Mbps = $100 \times 10^6=10^2 \times 10^6=10^8$ bps

		a) $\dfrac{10^8}{10^8}=10^{8-8}=10^0=1$

		b) $\dfrac{10^{10}}{10^8}=10^{10-8}=10^{2}=100$

??? question "Exercice 3"

    a) Calculer la métrique OSPF d'une liaison Ethernet (débit 10 Mbps) avec une valeur de référence de $10^8$ bps 

	b) Même question avec une valeur de référence de $10^{10}$ bps

    ??? success "Solution"

		10 Mbps = $10 \times 10^6=10 \times 10^6=10^7$ bps

		a) $\dfrac{10^8}{10^7}=10^{8-7}=10^1=10$

		b) $\dfrac{10^{10}}{10^7}=10^{10-7}=10^{3}=1000$

??? question "Exercice 4"

    Que vaut la bande passante d'une liaison dont le coût OSPF est de 50 avec une valeur de référence de $10^8$.

    ??? success "Solution"

		$50 = \dfrac{10^8}{x} \Leftrightarrow 50x=10^8 \Leftrightarrow x= \dfrac{10^8}{50} = \dfrac{100 \times 10^6}{50} = 2 \times 10^6$

		La bande passante est de 2 Mbps

        
### 3.2 Exemple

Reprenons le réseau suivant :

![image](images/tables.png){: .center  width=60%}


et simplifions-le en ne gardant que les liens entre routeurs, en indiquant leur débit :


![image](images/ospf1.png){: .center width=60%}


Notre réseau est devenu un **graphe**. 

Nous allons pondérer ses arêtes avec la fonction coût introduite précédemment. L'unité étant le Mbit/s, l'arête entre R1 et R3 aura un poids de $\frac{100}{20}=5$.

Le graphe pondéré est donc :


![image](images/ospf2.png){: .center width=60%}


Le chemin le plus rapide pour aller de l'ordinateur au serveur est donc R1-R2-R4, et non plus R1-R3 comme l'aurait indiqué le protocole RIP.


!!! question "Qu'est-ce que le protocole OSPF optimise ? :heart: :heart: :heart:"
    Le protocole OSPF cherche à minimiser **le temps de parcours**, en choisissant les lignes les plus rapides.


On peut donc retenir la différence fondamentale :
 
- RIP cherche le chemin le plus court.
- OSPF cherche le chemin le plus rapide.

## Bilan

<div class="centre" markdown="span">
    <iframe 
        src="{{ page.canonical_url }}../activites/voc_routage.html"
        width="900" height="800"
        id="quizz"
        title="Vocabulaire" 
        frameborder="0" 
        allow="autoplay; fullscreen; picture-in-picture">
    </iframe>
</div>
