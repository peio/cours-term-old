---
authors: Angélique Beucher, Jean-Louis Thirot, Valérie Mousseaux, Gilles Lassus et Mireille Coilhac
title: "Processus"
---

# Gestion des processus

## 1. Notion de processus

### 1.1 Définition d'un processus

!!! info "Définition"

    Un **processus** est un programme en cours d’exécution sur une machine.
    

    On dit que ce processus est une **instance d'exécution** de ce programme.

!!! attention "Attention"
    Il y a une différence entre programme et processus : 

    * le même programme peut être lancé plusieurs fois et donner naissances à des processus distincts ;
    * lorsqu'un programme est exécuté sur un ordinateur, celui-ci peut créer plusieurs processus.

!!! example "jeu vidéo"

    Un programme de jeu vidéo peut créer plusieurs processus lors de son exécution :

    * un processus pour la gestion des actions du joueurs,
    * un autre pour la modification des éléments du jeu,
    * un dernier pour dessiner les différents éléments à l'écran.

!!! info "Propriétés"

    Un processus est caractérisé par :

    * un ensemble d'instructions à exécuter - souvent stockées dans un fichier sur lequel on clique pour lancer un programme (par exemple firefox.exe)
    * un espace mémoire dédié à ce processus pour lui permettre de travailler sur des données et des ressources qui lui sont propres : si vous lancez deux instances de firefox, chacune travaillera indépendamment l'une de l'autre.
    * des ressources matérielles : processeur, entrées-sorties (accès à internet en utilisant la connexion Wifi).

    
!!! example "Différentes situations peuvent générer des processus :"

    * Double cliquer pour lancer un programme
    * Ouvrir un onglet dans un navigateur
    * Exécuter une commande dans une console


??? note "Analogie avec le domaine de la musique"

    Programme  ~  Partition 

    Processus  ~  Musicien 

    Processeur  ~  Instrument

???+ question "Un processus est :"

    === "Cocher la ou les affirmations correctes"
        
        - [ ] un programme exécutable
        - [ ] un logiciel
        - [ ] une instance d'exécution d'un programme      

    === "Solution"
        
        - :x:
        - :x:
        - :white_check_mark: une instance d'exécution d'un programme

???+ question "Un programme peut être exécuté :"

    === "Cocher la ou les affirmations correctes"
        
        - [ ] plusieurs fois à la suite par un même processus
        - [ ] plusieurs fois en même temps par un même processus
        - [ ] par plusieurs processus différents  

    === "Solution"
        
        - :x:
        - :x:
        - :white_check_mark: par plusieurs processus différents  


### 1.2 Observation des processus sous Linux

### 1.2.1 La commande `ps`

!!! info "La commande ps" 

    Dans un terminal, la commande ```ps``` va permettre d'afficher la liste des processus actifs. 

> Plus précisément, nous allons utiliser la commande ```ps -lu nom_user```. L'option ```l``` permet d'afficher un grand nombre de renseignements et l'option ```u``` permet de spécifier l'utilisateur propriétaire des processus.

![image](images/term1.png){: .center}

On retrouve notamment dans ce tableau les colonnes :

- ```CMD```  (Command) : le nom de la commande qui a créé le processus. Vous pouvez y retrouver par ordre chronologique le nom de tous les programmes actifs. Certains sont ceux que vous avez ouverts volontairement (navigateur...) mais on y trouve surtout tous les programmes nécessaires au bon fonctionnement du système d'exploitation. Le dernier processus en bas de la liste est forcément ```ps```, puisque vous venez de l'appeler.

- ```PID``` (Process Identifier) : le numéro unique d'identification, affecté chronologiquement par le système d'exploitation. Le processus de PID égal à 1 est ```systemd```, qui est le [tout premier programme](https://doc.ubuntu-fr.org/systemd){. target="_blank"} lancé par le noyau Linux au démarrage. 

- ```PPID``` (Parent PID) : certains processus vont *eux-mêmes* lancer plusieurs processus-fils, qui porteront le même nom. C'est ainsi qu'on peut retrouver de multiples processus s'appelant ```chrome``` :

![image](images/term_montage.png){: .center}

Ici, l'instance «principale» de Chrome (```PID``` 1453) a généré 6 autres instances de ```PID``` différents, dont le ```PPID``` vaut 1453.

> Dans Chrome/Chromium, vous pouvez comprendre le rôle de chaque processus en le retrouvant dans le gestionnaire des tâches (clic-droit sur une zone vide de la barre d'onglets)

!!! note "Interface graphique"

    Sur chaque OS, il y a des utilitaires en **mode graphique** (GUI) ou en **mode console** ( CLI ) pour observer les processus en cours d’exécution.

    Par exemple, on peut accéder aux processus sous Windows au travers du Gestionnaire des tâches.

### 1.2.2 La commande ```pstree``` 

!!! info "La commande pstree"

    À noter que la commande ```pstree``` permet d'afficher les processus sous forme d'arborescence : 

![image](images/pstree.png){: .center}

### 1.2.3 La commande ```top``` 

!!! abstract "La commande top"

    La commande ```top``` permet de connaître en temps réel la liste des processus, classés par ordre décroissant de consommation de CPU. 

On ferme ```top``` par la combinaison de touches ```Ctrl-C```.


!!! info "Pourcentage d'utilisation du CPU"
    En face de chaque processus est affichée sa consommation de CPU. Elle est calculée en prenant, sur un intervalle de temps donné, le temps qu'a passé le CPU à traiter le processus en question, et en divisant ce temps par le temps total de la mesure. 

    $$\text{Pourcentage d'utilisation CPU} = \frac{\text{Temps d'utilisation CPU}}{\text{Temps total écoulé}} \times 100$$

Si on repère alors un processus qui consomme beaucoup trop de ressources, on peut utiliser...

### 1.2.4 La commande ```kill``` 

!!! abstract "La commande kill"

    La commande ```kill``` permet de fermer un processus, en donnant son ```PID```  en argument.

!!! example "Exemple" 
     ```kill 1453``` tuera Chrome (voir la capture du 1.2.1)

!!! info "Fermeture multiple"

    Quand un programme ouvre plusieurs processus, fermer le processus père entrainera la fermeture de ses processus fils.

!!! example "Exemple" 
    Ainsi la commande ```kill 1453``` tuera le processus 1453, mais aussi ses processus fils : 1481, 1511, 1545, 1557, 1569, 1595
    



???+ question "Quelle commande permet de lister les processus ?"

    === "Cocher la ou les affirmations correctes"
        
        - [ ] ls
        - [ ] ps
        - [ ] sh
        - [ ] proc

    === "Solution"

        - :x:       
        - :white_check_mark: ps
        - :x:
        - :x:

???+ question "Quelle abréviation désigne l'identifiant d'un processus dans un système d'exploitation de type unix ?"

    === "Cocher la ou les affirmations correctes"
        
        - [ ] SIG
        - [ ] PID
        - [ ] SID

    === "Solution"
        
        - :x:
        - :white_check_mark: PID
        - :x:

???+ question "Quelle commande permet d'interrompre un processus dans un système d'exploitation de type unix ?"

    === "Cocher la ou les affirmations correctes"
        
        - [ ] stop
        - [ ] interrupt
        - [ ] end
        - [ ] kill

    === "Solution"
        
        - :x:
        - :x:
        - :x:
        - :white_check_mark: kill


## 2. Ordonnancement

### 2.1 Rappel sur les OS


!!! info "les systèmes d’exploitation"

    Le système d’exploitation, souvent appelé OS  ( Operating System ) est un ensemble de programmes qui permet d’utiliser les composants physiques d’un ordinateur pour exécuter toutes les applications dont l’utilisateur aura besoin.

    Toute machine est dotée d’un système d’exploitation qui a pour fonction de charger les programmes depuis la mémoire de masse et de lancer leur exécution en leur créant des processus, de gérer l’ensemble des ressources, de traiter les interruptions ainsi que les entrées-sorties et enfin d’assurer la sécurité globale du système.

!!! example "Exemples"

    Windows, Mac OS, Ubuntu Mate  sont des systèmes d’exploitation.

    Attention, Linux n’est pas à  proprement parlé  un OS , c’est en fait le noyau de nombreux OS  que l’on appelle des distributions Linux comme par exemple Gentoo, Debian, Linux Mint, Ubuntu, Fedora, RedHat … 


!!! abstract "Les tâches de l'OS"

    Le système d'exploitation doit allouer à chacun des processus les ressources dont il a besoin en termes de mémoire, entrées-sorties ou temps processeur, et de s'assurer que les processus ne se gênent pas les uns les autres.



!!! example "Gestion de la mémoire"

    La mémoire peut être vue comme un grand tableau disposant de “mots” repérés par des adresses. Si les applications écrivaient directement dans la mémoire, comme elles ne peuvent pas communiquer entre elles, voilà ce qui pourrait arriver :

    Une application écrit toto au mot 1  
    Une seconde application écrit NSI au même endroit

    😢 La seconde application a modifié une donnée dont l'application 1 a besoin....


!!! info "La mémoire virtuelle à la rescousse"

    Au démarrage d'une application, L'OS crée un espace de mémoire virtuelle dédié à cette application.

    L'application écrit dans la mémoire virtuelle. L'OS transfère ensuite dans la vraie mémoire et il mémorise l'information : si l'application demande l'information écrite au mot 1 de sa mémoire virtuelle il sait où trouver l'information dans la vraie mémoire.

### 2.2 Ordonnanceur

!!! warning
    Un ordinateur donne l'*illusion* de réaliser plusieurs tâches simultanément.
    
    Dans un système multitâches, plusieurs processus sont actifs simultanément, mais un processeur simple coeur ne peut exécuter qu’une instruction à la fois. 
    
    Il va donc falloir partager le temps de processeur disponible entre tous les processus.


!!! abstract "Ordonnanceur"

    L'**ordonnanceur** (ou **scheduler** en anglais) est un composant du système d'exploitation qui attribue au processus les ressources, notamment l'accès au CPU. 
    
    Si une application est bloquée (attente d'une donnée sur disque dur par exemple) l'ordonnanceur attribue le CPU à une autre application.



!!! info "Accès aux ressources :"

    * Les applications demandent à l'OS lorsqu'elle a besoin d'accéder à une les ressources (écran, CPU, fichier, etc...)

    * L'OS vérifie que l'application a les droits (voir, droits d'accès aux fichiers par exemple) et attribue l'accès ensuite. Cet accès peut être de deux types :
        * Privé : la ressource est attribuée et bloquée pour les autres applications, par exemple c'est le cas d'un accès en écriture dans un fichier.
        * Libre : la ressource est attribuée mais reste disponible pour les autres applications, par exemple c'est le cas d'un accès en lecture dans un fichier.


### 2.3 Les états d'un processus

!!! abstract "Les états d'un processus"

    Selon que l'ordonnanceur aura décidé de le confier ou non au processeur pour son exécution, un processus peut donc se trouver dans 3 états :

    - **Prêt** : le processus attend qu'arrive le moment de son exécution.
    - **Élu** : le processus est en cours d'exécution par le processeur.
    - **Bloqué** : pendant son exécution (état **Élu**), le processus réclame une ressource qui n'est pas immédiatement disponible. Son exécution s'interrompt. Lorsque la ressource sera disponible, le processus repassera par l'état **Prêt** et attendra à nouveau son tour. 



Ces processus vont devoir s’exécuter à tours de rôle, pour gérer l’ordre d’exécution lesystème d’exploitation va attribuer des états aux processus.



!!! note "Pourquoi l'accès à une ressource peut bloquer un processus ?"

    Pendant son exécution, un processus peut avoir besoin d'accéder à une ressource déjà occupée (un fichier déjà ouvert, par exemple) ou être en attente d'une entrée-utilisateur (un `input()` dans un code Python par exemple). Dans ce cas-là, le processeur va passer ce processus à l'état **Bloqué**, pour pouvoir ainsi se consacrer à un autre processus.

    Une fois débloqué, le processus va repasser à l'état **Prêt** et rejoindre (par exemple) la file d'attente des processus avant d'être à nouveau **Élu** et donc exécuté.

!!! question "Schéma"

    A partir des indications précédentes, compléter le schéma suivant :
    <iframe id="etats"
        title="Etats d'un processus"
        width="600"
        height="550"
        src="{{ page.canonical_url }}../activites/etat.html">
    </iframe>

???+ question "A un instant donné un processeur exécute :"

    === "Cocher la ou les affirmations correctes"
        
        - [ ] tous les processus prêts
        - [ ] le processus élu
        - [ ] tous les processus d'un utilisateurs

    === "Solution"
        
        - :x:
        - :white_check_mark: le processus élu
        - :x:

???+ question "Un processus prêt :"

    === "Cocher la ou les affirmations correctes"
        
        - [ ] est exécuté
        - [ ] a été exécuté dans le processeur
        - [ ] attend d'obtenir le processeur    

    === "Solution"
        
        - :x:
        - :x:
        - :white_check_mark: attend d'obtenir le processeur  

    
### 2.4 L'ordonnancement des processus


Si on vous donne 4 tâches A, B, C et D à accomplir, vous pouvez décider :

- de faire la tâche prioritaire d'abord ;
- de faire la tâche la plus rapide d'abord ;
- de faire la tâche la plus longue d'abord ;
- de les faire dans l'ordre où elles vous ont été données ;
- de faire à tour de rôle chaque tâche pendant un temps fixe jusqu'à ce qu'elles soient toutes terminées;
- ...

Un processeur est confronté aux mêmes choix : comment déterminer quel processus doit être traité à quel moment ?

!!! info "Ordonnancement"

	Afin de choisir quel processus va repasser en mode exécution, l'ordonnanceur applique un algorithme prédéfini lors de la conception de l'OS. Le choix que va réaliser cet algorithme va impacter directement la réactivité du système et les usages qui pourront en être fait. C'est un élément critique du système d'exploitation.


!!! note "Multicoeur"
    
    Dans le cas (très fréquent maintenant) d'un processeur *multi-cœurs*, le problème reste identique. 
    
    Certes, sur 4 cœurs, 4 processus pourront être traités simultanément (une **réelle** simultanéité) mais il reste toujours beaucoup plus de processus à traiter que de cœurs dans le processeur... et un ordonnancement est donc toujours nécessaire.



!!! abstract "Les différents algorithmes d'ordonnancement"

    Il existe différents algorithmes comme 
    * le modèle **FIFO** : on affecte les processus dans l'ordre de leur apparition dans la file d'attente.
	* Le modèle **SJF** (Shortest Job First) : on affecte en premier le « plus court processus en premier » de la file d'attente à l'unité de calcul.
	* Le modèle **Priorité** : chaque processus dispose d’une valeur de priorité et on choisit le processus de plus forte priorité à chaque fois.




!!! note "L'algorithme du tourniquet"

    Sous Linux, l'ordonnancement est effectué par un système hybride où les processus sont exécutés à tour de rôle (on parle de *tourniquet* ou de *Round Robin*) suivant un ordre de priorité dynamique :

    * Les processus prêts à être exécutés sont placés dans une file d’attente selon leur ordre d’arrivée ;
    * L’ordonnanceur alloue le processeur à chaque processus de la file d’attente un même nombre de cycles CPU ;
    * Si le processus n’est pas terminé au bout de ce temps, son exécution est suspendue et il est mis à la fin de la file d’attente ;
    * Si le processus est terminé, il sort définitivement de la file d’attente.

    ![processus](images/Round_Robin_Schedule_Example.jpeg)

!!! abstract "Le quantum"

	L'algorithme alloue le processeur au processus élu pendant une unité de temps appelée  **quantum**.

    Ce quantum est fixé en nombre de cycles du CPU.

<iframe id="ordonnancement"
    title="Ordonnancement des processus"
    width="700"
    height="500"
    src="{{ page.canonical_url }}../activites/ordonnancement.html">
</iframe>



???+ question "L'ordonnanceur :"

    === "Cocher la ou les affirmations correctes"
        
        - [ ] donne des instructions pour réparer des processus cassés
        - [ ] transforme un programme en processus
        - [ ] planifie l'exécution des processus


    === "Solution"
        
        - :x:
		- :x:
        - :white_check_mark: planifie l'exécution des processus

    
???+ question "Un processus"

    === "Cocher la ou les affirmations correctes"
        
        - [ ] ne s'exécute jamais en même temps qu'un autre processus sur le même processeur.
        - [ ] peut être mis en attente par l'ordonnancement du système d'exploitation
        - [ ] réalise l'ensemble des ses instructions avant de rendre la main

    === "Solution"
        
        - :white_check_mark: ne s'exécute jamais en même temps qu'un autre processus sur le même processeur.
		- :white_check_mark: peut être mis en attente par l'ordonnancement du système d'exploitation
        - :x:
		
???+ question "Un processus en cours d'exécution est dit :"

    === "Cocher la ou les affirmations correctes"
        
        - [ ] élu
        - [ ] bloqué
        - [ ] prêt
        - [ ] actif

    === "Solution"
        
        - :white_check_mark: élu
        - :x:
        - :x:
        - :x:


???+ question "Un processus bloqué :"

    === "Cocher la ou les affirmations correctes"
        
        - [ ] ne peut plus s'exécuter
        - [ ] a été exécuté dans le processeur
        - [ ] attend d'obtenir le processeur    

    === "Solution"
        
        - :x:
        - :x:
        - :white_check_mark: attend d'obtenir le processeur  





## 3. Interblocage


Comme nous venons de le voir, un processus peut être dans l'état bloqué dans l'attente de la libération d'une ressource. 

!!! note "verrou"

    Les ressources matérielles (l'accès en écriture à un fichier, à un registre de la mémoire...) ne peuvent être données à deux processus à la fois. 
    Des processus souhaitant accéder à cette ressource sont donc en **concurrence** sur cette ressource.

    Lorsqu'un processus utilise une ressource, il pose un **verrou** dessus.
    Un autre processus ne pourra utiliser cette ressource que lorsque le verrou sera oté et la ressource libérée.
    

Lorsque l'on utilise plusieurs verrous pour accéder à des ressources, il peut y avoir une situation d'interblocage
La situation d'interblocage se produit lorsqu'il y a un cycle dans les dépendances sur les ressources critiques

!!! note "Conditions"

    Il faut 4 conditions pour un interblocage :

    * Les ressources utilisées sont en exclusion mutuelles (un seul accès)
    * Chaque processus utilise simultanément plusieurs ressouces acquises au fur et à mesure des besoins, sans libérer celles qu'il possède déjà
    * La réservation des ressources est potentiellement bloquante
    * Il existe un cycle de processus ou chaque processus attend une ressource possédée par le processus suivant du cycle.





!!! example "Exemple d'interblocage"

	Supposons un système possédant les ressources 1 et 2, ayant les processus A et B en exécution.

	Supposons aussi que les ressources ne sont pas partageables : un seul processus à la fois peut utiliser une ressource.

	Une façon de provoquer un interblocage est la suivante :

	* Le processus A utilise la ressource 1;
	* Le processus B utilise la ressource 2;
	* Le processus A demande la ressource 2, et devient à l'état bloqué.
	* Le processus B demande la ressource 1, et devient à l'état bloqué.

    ![image](images/tab_proc.png){: .center}

    Les deux processus A et B sont donc dans l'état **Bloqué**, chacun en attente de la libération d'une ressource bloquée par l'autre : ils se bloquent mutuellement.


!!! abstract "Interblocage ou deadlock"

    En informatique, cette situation (critique) appelée **interblocage** ou **deadlock** survient quand des processus convoitent les mêmes ressources.  Les processus bloqués dans cet état le sont définitivement.



!!! question "Exercice"

    <iframe id="interblocage2"
        title="interblocage"
        width="600"
        height="900"
        src="{{ page.canonical_url }}../activites/interblocage.html">
    </iframe>

!!! example "Pour aller plus loin"
    [Exemple d'un interblocage](http://lycee.educinfo.org/index.php?page=interblocage&activite=processus){ .md-button }




## IV. Questions Flash

<div class="centre" markdown="span">
    <iframe 
        src="{{ page.canonical_url }}../activites/processus.html"
        width="900" height="800"
        id="quizz"
        title="Vocabulaire" 
        frameborder="0" 
        allow="autoplay; fullscreen; picture-in-picture">
    </iframe>
</div>

